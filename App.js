import React, { useEffect, useState } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';
import { store, persistor } from './src/Redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';
import Navigation from './src/Routes/Navigation';
import Explore from './src/Views/Explore/Explore';
import ScheduleForLater from './src/Views/ScheduleForLater/ScheduleForLater';
import AddParking from './src/Views/AddParking/AddParking';
// import Start from './src/Views/CarpoolPage/Start/Start';
// import Carpool from './src/Views/CarpoolPage/Carpool/Carpool'; 
import Demo from './src/Views/Salunke/Demo';
import Sfl from './src/Views/Salunke/Sfl';
import ParkingSpot from './src/Views/Parking/ParkingSpot';
import BackBtn from './src/Components/gomzy/BackBtn';
import Licence from './src/Views/gomzy/Licence';
import NearBy from './src/Views/gomzy/NearBy';
import Vehicle from './src/Views/gomzy/Vehicle';
import SignUp from './src/Views/Auth/SignUp';
import FileUploader from "./src/Views/FileUploader/FileUploader"
import CarpoolPage from './src/Views/CarpoolPage/CarpoolPage';
import ForgetPassword from './src/Views/ForgetPassword/ForgetPassword'
import AddVehicle from './src/Views/AddVehicle/AddVehicle';
import VehicleList from './src/Views/VehicleList/VehicleList';
import ParkingSpots from './src/Views/ParkingSpots/ParkingSpots';
import VehicleAdd from './src/Views/Vehicle/VehicleAdd';


const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <PaperProvider>
          {/* <Explore /> */}
          {/* <ScheduleForLater /> */}
          {/* <AddParking /> */}
          {/* <Start /> */}
          {/* <CarpoolPage /> */} 
          {/* <Demo /> */}
          {/* <Sfl /> */}
          {/* <ParkingSpot /> */}
          {/* <Carpool /> */}
          {/* <Licence /> */}
          {/* <NearBy /> */}
          {/* <Vehicle /> */}
          {/* <SignUp /> */}
          {/* <FileUploader /> */}
          {/* <ForgetPassword /> */}
          {/* <AddVehicle /> */}
          {/* <VehicleList /> */}
          {/* <ParkingSpots /> */}
          {/* <AddParking /> */}
          {/* <VehicleAdd /> */}

          <Navigation />

        </PaperProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
