export { default as Location } from "../icons/ic_loc.svg";
export { default as Car } from "../icons/fluent_vehicle-car-profile-ltr-16-filled.svg"; 
export { default as Offline } from "../icons/heroicons-solid_status-offline.svg";
export { default as Online } from "../icons/heroicons-solid_status-online.svg"; 
export {default as HomeYellow } from "../icons/Iconly/Bold/Home.svg";
export {default as Home } from "../icons/Iconly/Bold/Home-1.svg";
export {default as Send } from "../icons/Iconly/Bold/Send.svg"
export {default as Category } from "../icons/Iconly/Light/Category.svg"
export {default as Profile } from "../icons/Iconly/Light/Profile.svg"
export {default as Search } from "../icons/Iconly/Light/Search.svg"

