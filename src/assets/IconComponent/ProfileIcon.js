import React from 'react';
import {View, Text} from 'react-native';
import Svg, {Path, Circle} from 'react-native-svg';

const ProfileIcon = props => {
  return (
    <Svg
    width={props.width}
    height={props.height}
    viewBox="0 0 30 30"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <Path
      clipRule="evenodd"
      d="M14.981 19.183c-4.835 0-8.963.73-8.963 3.658 0 2.927 4.102 3.685 8.963 3.685 4.835 0 8.962-.732 8.962-3.659 0-2.926-4.101-3.684-8.962-3.684zM14.981 15.007a5.745 5.745 0 10-5.745-5.745 5.726 5.726 0 005.704 5.745h.041z"
    />
  </Svg>
  );
};

export default ProfileIcon;
