import React from 'react';
import {View, Text} from 'react-native';
import Svg, {Path, Circle} from 'react-native-svg';

const SendIcon = props => {
  return (
    <Svg
    height={props.height}
    width={props.width}
      viewBox="0 0 31 31"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M27.366 3.656a2.416 2.416 0 00-2.414-.63L4.832 8.879c-.911.253-1.556.979-1.73 1.901-.178.939.443 2.13 1.253 2.629l6.291 3.867a1.63 1.63 0 002.012-.242l7.205-7.25a.917.917 0 011.325.001.953.953 0 010 1.334l-7.217 7.25a1.654 1.654 0 00-.24 2.023l3.843 6.355a2.388 2.388 0 002.076 1.183c.1 0 .213 0 .313-.013A2.437 2.437 0 0022 26.18l5.965-20.095a2.455 2.455 0 00-.6-2.429z"
        fill="#fff"
      />
    </Svg>
  );
};

export default SendIcon;




