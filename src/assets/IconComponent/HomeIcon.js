import React from 'react';
import {View, Text} from 'react-native';
import Svg, {Path, Circle} from 'react-native-svg';

const HomeIcon = props => {
  return (
    <Svg
      height={props.height}
      width={props.width}
    //   {props.color}
      viewBox="0 0 25 25"
    //   fill={props.color}
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path
        d="M9.515 21.639v-3.185c0-.813.664-1.473 1.483-1.473h2.994c.394 0 .77.156 1.049.432.278.276.434.65.434 1.04v3.186c-.002.338.131.663.371.903s.567.375.907.375h2.043a3.604 3.604 0 002.545-1.041 3.552 3.552 0 001.055-2.524v-9.074c0-.765-.342-1.49-.933-1.981l-6.948-5.51a3.226 3.226 0 00-4.114.074l-6.79 5.436a2.577 2.577 0 00-1.007 1.981v9.065c0 1.974 1.612 3.574 3.6 3.574H8.2c.708 0 1.282-.567 1.287-1.269l.028-.01z"
        fill={props.color}
        // stroke="black"
      />
    </Svg>
  );
};

export default HomeIcon;
