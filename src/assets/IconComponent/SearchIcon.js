import React from 'react';
import {View, Text} from 'react-native';
import Svg, {Path, Circle} from 'react-native-svg';

const SearchIcon = props => {
  return (
    <Svg
      width={props.width}
      height={props.height}
      viewBox="0 0 30 30"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Circle
        cx={14.708}
        cy={14.708}
        r={11.236}
        stroke="#130F26"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M22.523 23.106l4.405 4.394"
        stroke="#130F26"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};

export default SearchIcon;






