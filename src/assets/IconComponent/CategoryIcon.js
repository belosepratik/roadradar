import React from 'react';
import {View, Text} from 'react-native';
import Svg, {Path, Circle} from 'react-native-svg';

const CategoryIcon = props => {
  return (
    <Svg
    width={props.width}
    height={props.height}
    viewBox="0 0 28 28"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <Path
      clipRule="evenodd"
      d="M3.5 7.583c0-3.062.033-4.083 4.083-4.083s4.084 1.02 4.084 4.083.013 4.084-4.084 4.084c-4.096 0-4.083-1.02-4.083-4.084zM16.333 7.583c0-3.062.033-4.083 4.084-4.083 4.05 0 4.083 1.02 4.083 4.083s.013 4.084-4.083 4.084c-4.097 0-4.084-1.02-4.084-4.084zM3.5 20.417c0-3.063.033-4.084 4.083-4.084s4.084 1.02 4.084 4.084c0 3.062.013 4.083-4.084 4.083-4.096 0-4.083-1.02-4.083-4.083zM16.333 20.417c0-3.063.033-4.084 4.084-4.084 4.05 0 4.083 1.02 4.083 4.084 0 3.062.013 4.083-4.083 4.083-4.097 0-4.084-1.02-4.084-4.083z"
    />
  </Svg>
  );
};

export default CategoryIcon;
