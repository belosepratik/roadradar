import React from 'react';
import {View, Text} from 'react-native';
import Svg, {Path, Circle} from 'react-native-svg';

const OnlineIcon = props => {
  return (
<Svg
      width={35}
      height={35}
      viewBox="0 0 35 35"
    //   fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.838 6.363a1.75 1.75 0 010 2.475 12.25 12.25 0 000 17.325 1.75 1.75 0 11-2.475 2.474 15.75 15.75 0 010-22.274 1.75 1.75 0 012.475 0zm17.325 0a1.75 1.75 0 012.474 0 15.749 15.749 0 010 22.274 1.749 1.749 0 01-3.009-1.23 1.75 1.75 0 01.535-1.244 12.249 12.249 0 000-17.325 1.75 1.75 0 010-2.475zm-12.375 4.949a1.75 1.75 0 010 2.475 5.25 5.25 0 000 7.425 1.752 1.752 0 01-1.239 2.987 1.752 1.752 0 01-1.237-.513 8.75 8.75 0 010-12.372 1.75 1.75 0 012.476 0v-.002zm7.424 0a1.749 1.749 0 012.476 0 8.751 8.751 0 010 12.376 1.75 1.75 0 11-2.476-2.476 5.25 5.25 0 000-7.424 1.752 1.752 0 010-2.476zM17.5 15.75a1.75 1.75 0 011.75 1.75v.017a1.75 1.75 0 11-3.5 0V17.5a1.75 1.75 0 011.75-1.75z"
        fill={props.color}
      />
    </Svg>
  );
};

export default OnlineIcon;
