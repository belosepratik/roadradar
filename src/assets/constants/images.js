export const car = require('../constants/car1.png');
export const bus = require('../constants/bus3.png');
export const jeep = require('../constants/jeep1.png');
export const cab = require('../constants/cab1.png');
export const metro = require('../constants/metro1.png');
export const suv = require('../constants/SUV1.png');
export const train = require('../constants/train2.png');
export const bike = require('../constants/motorbike1.png');
export const scooter = require('../constants/scooter1.png');
export const cycle = require('../constants/bicycle1.png');
export const pills = require('../constants/pills.png');
export const carpool1 = require('../constants/carpool1.png');
export const pothole3 = require('../constants/pothole3.png');
export const potholeelement = require('../constants/potholeelement.png');
export const explore = require('../constants/explore.png');
export const speedcheck = require('../constants/speedcheck.png');
export const cab10 = require('../constants/cab10.png');
export const car10 = require('../constants/car10.png');
export const startloc = require('../constants/startloc.png');
export const connector = require('../constants/connector.png');
export const endloc = require('../constants/endloc.png');
