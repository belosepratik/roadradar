import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import BottomTabs from '../BottomTabs/ExploreTabs';
import DrawerContent from './DrawerContent';
import Home from '../../Views/HomeScreens/Home';
import { createStackNavigator } from '@react-navigation/stack';
import OnBoarding from '../../Views/OnBoarding/OnBoarding'
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
export default function Drawers(props) {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {/* <Stack.Screen name="OnBoarding" component={OnBoarding} /> */}
      <Stack.Screen name="Tabs" component={BottomTabs} />
      {/* <Stack.Screen name="Register" component={Register} /> */}
    </Stack.Navigator>
  );
}
