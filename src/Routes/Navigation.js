import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Drawers from './Drawers/Drawers';
import AuthStacks from './Stacks/AuthStacks/AuthStacks';
import { useSelector } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import OnBoarding from '../Views/OnBoarding/OnBoarding';
import SplashScreen from '../Views/SplashScreen';
import Gtr from '../Views/Salunke/Gtr';
import Vehicle from '../Views/gomzy/Vehicle';
import Destinatination from '../Views/OnBoarding/Destinatination';
import OnBoardingSpeed from '../Components/AppComponents/OnBoardingComponent/OnBoardingSpeed';
import Explore from '../Views/Explore/Explore';
import OnBoardingDest from '../Components/AppComponents/OnBoardingComponent/OnBoardingDest';
import ScheduleForLater from '../Views/ScheduleForLater/ScheduleForLater'
import Sfl from '../Views/Salunke/Sfl';
import SFLTimer from '../Views/SFL/SFLTimer';
import CarpoolPage from '../Views/CarpoolPage/CarpoolPage';
import Carpool from '../Views/gomzy/Carpool';
import OnBoardingsilide3 from '../Components/AppComponents/OnBoardingComponent/OnBoardingsilide3';
import NearBy from '../Views/gomzy/NearBy';
import ForgetPassword from '../Views/ForgetPassword/ForgetPassword';
import AddVehicle from '../Views/AddVehicle/AddVehicle';
import VehicleList from '../Views/VehicleList/VehicleList';
import ParkingSpots from '../Views/ParkingSpots/ParkingSpots';
import AddParking from '../Views/AddParking/AddParking';
import Speed from '../Views/Speed/Speed';
import ExploreTabs from '../Routes/BottomTabs/ExploreTabs';

const Stack = createStackNavigator();

export default function Navigation() {
  const login = useSelector((state) => state.login);
  const { isLogged } = login;
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{ headerShown: false }}
        initialRouteName="SplashScreen">
        <Stack.Screen name="SplashScreen" component={SplashScreen} />

        <Stack.Screen name="OnBoarding" component={OnBoarding} />
        <Stack.Screen name="OnGetting" component={Gtr} />
        <Stack.Screen name="OnVehicle" component={Vehicle} />
        <Stack.Screen name="OnDestinatination" component={Destinatination} />
        <Stack.Screen name="OnCarpoolAdd" component={CarpoolPage} />
        <Stack.Screen name="Carpool" component={Carpool} />
        <Stack.Screen name="OnSpeed" component={OnBoardingSpeed} />
        <Stack.Screen name="OBDest" component={OnBoardingDest} />
        <Stack.Screen name="ScheduleForLater" component={ScheduleForLater} />
        {/* <Stack.Screen name="Sfl" component={Sfl} /> */}
        <Stack.Screen name="SFLTimer" component={SFLTimer} />
        <Stack.Screen name="NearBy" component={NearBy} />
        <Stack.Screen name="ForgetPassword" component={ForgetPassword} />
        <Stack.Screen name="AddVehicle" component={AddVehicle} />
        <Stack.Screen name="VehicleList" component={VehicleList} />
        <Stack.Screen name="ParkingSpots" component={ParkingSpots} />
        <Stack.Screen name="AddParking" component={AddParking} />
        <Stack.Screen name="Speed" component={Speed} />

 
        <Stack.Screen name="ExploreTabs" component={ExploreTabs} />
        <Stack.Screen name="Explore" component={Explore} />


        <Stack.Screen name="Auth" component={AuthStacks} />
        <Stack.Screen name="Home" component={Drawers} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
