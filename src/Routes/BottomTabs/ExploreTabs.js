import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeStack from '../Stacks/HomeStacks/HomeStack';
import SearchStack from '../Stacks/SearchStacks/SearchStack';
import Icon from 'react-native-vector-icons/AntDesign';
import Profile from '../../Views/ProfileScreen/Profile';
import ProfileStacks from '../Stacks/ProfileStacks/ProfileStacks';
import HomeIcon from '../../assets/IconComponent/HomeIcon';
import SendIcon from '../../assets/IconComponent/SendIcon';
import CategoryIcon from '../../assets/IconComponent/CategoryIcon';
import ProfileIcon from '../../assets/IconComponent/ProfileIcon';
import SearchIcon from '../../assets/IconComponent/SearchIcon';
import CategoryStacks from '../Stacks/CategoryStacks/CategoryStacks';
import Explore from '../../Views/Explore/Explore';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import AntDesign from 'react-native-vector-icons/AntDesign';

const Tab = createBottomTabNavigator();

export default function ExploreTabs({ navigation, route }) {



  return (
    <Tab.Navigator
      // screenOptions={{
      //   headerShown: false
      // }}
      // initialRouteName="Explore"
      // tabBarOptions={{
      //   keyboardHidesTabBar: true,
      //   showLabel: false,
      //   activeTintColor: '#ffdc4a', 
      // }}

      initialRouteName="Explore"
      screenOptions={{
        tabBarLabel: "",
        headerShown: false,
        keyboardHidesTabBar: true,
      }}

    >
      <Tab.Screen
        name="CategoryStacks"
        component={Explore}
        options={{
          tabBarIcon: ({ color, size, tintColor }) => (
            <AntDesign name="home" size={30} />
          ),
          //   tabBarBadge: 5,
        }}
      />

      <Tab.Screen
        name="Explore"
        component={Explore}
        options={{
          tabBarIcon: ({ color, size, tintColor }) => (
            <MCI name="calendar-remove-outline" size={30} />
          ),
          // tabBarBadge: 3,
        }}
      />
      <Tab.Screen
        name="HomeStack"
        component={Explore}
        options={{
          tabBarIcon: ({ color, size, tintColor }) => (
            <Octicons name="diff-added" size={30} />
          ),
          // tabBarBadge: 3,
        }}
      />
      <Tab.Screen
        name="DomainForm"
        component={Explore}
        options={{
          tabBarIcon: ({ color, size, tintColor }) => (
            <AntDesign name="user" size={30} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
