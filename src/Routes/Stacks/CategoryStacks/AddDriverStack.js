import React from 'react'

import {createStackNavigator} from '@react-navigation/stack';
import AddDriver from '../../../Views/CategoryScreen/AddDriver';

const Stack = createStackNavigator();
export default function AddDriverStack() {
    return (
        // define here all Home tabs route here
        <Stack.Navigator initialRouteName="AddDriverScreen" screenOptions={{headerShown: false}}>
        <Stack.Screen name="AddDriverScreen" component={AddDriver} /> 
      </Stack.Navigator>
    )
}
