import React from 'react'

import {createStackNavigator} from '@react-navigation/stack';
import Category from '../../../Views/CategoryScreen/Category';
import AddVehicle from '../../../Views/CategoryScreen/AddVehicle';
import AddDriver from '../../../Views/CategoryScreen/AddDriver';

const Stack = createStackNavigator();
export default function CategoryStacks() {
    return (
        // define here all Home tabs route here
        <Stack.Navigator initialRouteName="CategoryScreen" screenOptions={{headerShown: false}}>
        <Stack.Screen name="CategoryScreen" component={Category} /> 
        <Stack.Screen name="AddVehicleScreen" component={AddVehicle} />
        <Stack.Screen name="AddDriverScreen" component={AddDriver} />
      </Stack.Navigator>
    )
}
