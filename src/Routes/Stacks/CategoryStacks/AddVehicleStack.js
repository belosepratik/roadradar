import React from 'react'

import {createStackNavigator} from '@react-navigation/stack';
import AddVehicle from '../../../Views/CategoryScreen/AddVehicle';

const Stack = createStackNavigator();
export default function AddVehicleStack() {
    return (
        // define here all Home tabs route here
        <Stack.Navigator initialRouteName="AddVehicleScreen" screenOptions={{headerShown: false}}>
        <Stack.Screen name="AddVehicleScreen" component={AddVehicle} /> 
      </Stack.Navigator>
    )
}
