import React from 'react'

import {createStackNavigator} from '@react-navigation/stack';
import Search from '../../../Views/SearchScreen/Search';

const Stack = createStackNavigator();
export default function HomeStack() {
    return (
        // define here all Home tabs route here
        <Stack.Navigator initialRouteName="SearchScreen" screenOptions={{headerShown: false}}>
        <Stack.Screen name="SearchScreen" component={Search} /> 
      </Stack.Navigator>
    )
}
