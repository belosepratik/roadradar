import Geocoder from 'react-native-geocoding';


export const GeoDecoding = async (lat, long, apikey) => {

    Geocoder.init(apikey);
    const json = await Geocoder.from([lat, long])
    console.log(json)
    return json.results[2].address_components[2].long_name;
}



