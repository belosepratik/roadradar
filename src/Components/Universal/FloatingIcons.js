import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {TouchableOpacity, Image} from 'react-native';
import {Text} from 'native-base';

export const FloatingIcons = ({bottomPosition, icon, navigation, onHandle}) => {
  return (
    <TouchableOpacity
      onPress={() => {
        onHandle();
      }}
      style={{
        position: 'absolute',
        zIndex: 2,
        backgroundColor: 'white',
        height: 50,
        width: 50,
        right: '4%',
        bottom: bottomPosition,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
      }}>
      {icon == 'speed' && (
        // <MaterialCommunityIcons
        //   name="speedometer-medium"
        //   size={25}
        //   color="black"
        // />
        <Image source={require('../../../src/assets/speed-check.png')}></Image>
      )}
      {icon == 'globe' && (
        // <MaterialCommunityIcons
        //   name="rocket-launch-outline"
        //   size={25}
        //   color="black"
        // />
        <Image source={require('../../../src/assets/explore.png')}></Image>
      )}
    </TouchableOpacity>
  );
};
