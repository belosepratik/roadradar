import React from 'react'
import Backarrow from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native'

export const BackButton = ({ navigation, route }) => {
    return (
        <TouchableOpacity
            onPress={() => { navigation.goBack() }}
            style={{
                position: 'absolute',
                zIndex: 2,
                backgroundColor: 'white',
                height: 50,
                width: 50,
                left: '4%',
                top: '2%',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10
            }}>
            <Backarrow
                name="arrow-back"
                size={25}
                color="black"
            />
        </TouchableOpacity>
    )
}
