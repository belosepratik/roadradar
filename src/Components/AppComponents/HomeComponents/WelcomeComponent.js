import { Button } from 'native-base';
import React, {useEffect, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Dimensions,
} from 'react-native';

const {width} = Dimensions.get('window');
const height = width * 0.4;

const WelcomeComponent = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);

  return (
    <View style={{backgroundColor: 'white', margin: 10}}>
      <View>
        <Text style={{fontSize: 20, fontWeight: 'bold'}}>Welcome</Text>
        <Text style={{color: 'grey'}}>Let's Choose Your</Text>
        <ScrollView
          pagingEnabled
          horizontal
          style={{width, height, alignSelf: 'center', padding: 10}}>
          <TouchableOpacity onPress={(e)=>{
              alert("Selected 1st option")
          }}>
            
            <Image
              source={require('../../../assets/lambo.jpg')}
              style={{
                width,
                height,
                alignSelf: 'center',
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={(e)=>{
              alert("Selected 2nd option")
          }}>
            <Image
              source={require('../../../assets/driver.jpg')}
              style={{
                width,
                height,
                alignSelf: 'center',
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={(e)=>{
              alert("Selected 3rd option")
          }}>
            <Image
              source={require('../../../assets/lambo.jpg')}
              style={{
                width,
                height,
                alignSelf: 'center',
                resizeMode: 'contain',
              }}
            />
            
          </TouchableOpacity>
        </ScrollView>

        <TouchableOpacity style={{backgroundColor: '#1E8FE7', padding: 10}}>
          <Text style={{alignSelf: 'center', color: 'white'}}>Choose </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default WelcomeComponent;

const styles = StyleSheet.create({});
