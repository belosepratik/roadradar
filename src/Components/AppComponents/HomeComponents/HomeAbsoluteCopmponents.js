
import React from 'react'
import { Text, View, StyleSheet, TextInput, Pressable } from 'react-native'
import Geocoder from 'react-native-geocoding';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import SwitchSelector from 'react-native-switch-selector'
import { useDispatch, useSelector } from 'react-redux';
import { MAP_KEY } from '../../../../Config';
import { setdestlatlong, setDestLocation } from '../../../Redux/Slice/HomeSlice';

export default function HomeAbsoluteCopmponents({ wifi, wifi1, off, traveloptions }) {
    const home = useSelector(state => state.home)
    const { myLocation } = home
    const dispatch = useDispatch()
    return (
        <View>
            <View>
                <SwitchSelector
                    style={[styles1.buttonstyle2], { width: '50%', alignSelf: 'center', marginTop: 20 }}
                    buttonColor={'#04b400'}
                    selectedColor={'#7a44cf'}
                    rajHeight={70}
                    rajWidth={80}
                    borderRadius={50}
                    options={[
                        { label: '', value: 'f', customIcon: wifi },
                        { label: '', value: 'm', customIcon: wifi1 },
                        { label: '', value: 'o', customIcon: off },
                    ]}
                    initial={0}
                    onPress={value => console.log(`Call onPress with value: ${value}`)}
                />
            </View>


            <View style={styles1.searchBtn}>
                <Pressable style={{ width: '100%' }}>
                    <View>
                        <TextInput
                            style={styles1.textInput}
                            placeholder="From"
                            // placeholderTextColor="gray"
                            value={myLocation}
                        // selection={key}
                        />
                        <View>
                            <GooglePlacesAutocomplete
                                styles={{
                                    textInputContainer: {
                                        marginLeft: 20,
                                        marginTop: 0,
                                    },
                                    textInput: {
                                        borderRadius: 0,
                                        color: 'grey'
                                    },
                                }}
                                placeholder='Search'
                                onPress={(data, details = null) => {
                                    Geocoder.init(MAP_KEY);
                                    dispatch(setDestLocation(data.description))
                                    Geocoder.from(data.description)
                                        .then(json => {
                                            var location = json.results[0].geometry.location;
                                            // console.log(location);
                                            dispatch(setdestlatlong(location))
                                        })
                                        .catch(error => console.warn(error))
                                    // 'details' is provided when fetchDetails = true
                                    // console.log(data.description, details);
                                }}
                                query={{
                                    key: MAP_KEY,
                                    language: 'en',
                                }}
                            />
                        </View>
                        <View style={styles1.circle}></View>
                        {/* Line between dots */}
                        <View style={styles1.line}></View>
                        {/* Square near Destination Input */}
                        <View style={styles1.square}></View>
                        <View
                            style={{
                                width: '100%',
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                            <SwitchSelector
                                style={styles1.buttonstyle}
                                buttonColor={'#ffdc4a'}
                                borderRadius={50}
                                rajHeight={40}
                                rajWidth={80}
                                options={traveloptions}
                                initial={0}
                                onPress={value =>
                                    console.log(`Call onPress with value: ${value}`)
                                }
                            />
                        </View>
                        {/* </Pressable> */}
                    </View>
                </Pressable>

            </View>


        </View>
    )
}


const styles1 = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        // backgroundColor: 'black',
    },
    searchBox: {
        marginTop: Platform.OS === 'ios' ? 40 : 10,
        flexDirection: 'row',
        // backgroundColor: '#fff',
        // width: '90%',
        // alignSelf:'center',
        borderRadius: 5,
        paddingVertical: 20,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        // elevation: 10,
    },
    ham: {
        marginTop: Platform.OS === 'ios' ? 40 : 20,
        flexDirection: 'row',
        backgroundColor: '#fff',
        // width: '90%',
        // alignSelf:'center',
        borderRadius: 5,
        padding: 10,
        margin: 10,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    },
    searchBtn: {
        marginTop: Platform.OS === 'ios' ? 200 : 20,
        flexDirection: 'row',
        backgroundColor: '#fff',
        width: '90%',
        alignSelf: 'center',
        borderRadius: 15,
        padding: 10,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    },
    autoCompleteContainer: {

        top: 22,
        left: 10,
        right: 10,
    },
    textInput: {
        padding: 10,
        width: '90%',
        // backgroundColor: '#eee',
        color: 'gray',
        marginVertical: 5,
        marginLeft: 20,
    },
    circle: {
        width: 10,
        height: 10,
        backgroundColor: '#ffdc4a',
        position: 'absolute',
        top: 20,
        left: 5,
        borderRadius: 5,
    },
    line: {
        width: 1,
        height: 45,
        backgroundColor: '#c4c4c4',
        position: 'absolute',
        top: 40,
        left: 10,
        borderRadius: 5,
    },
    square: {
        top: -20,
        left: 5,
        width: 0,
        height: 0,
        borderLeftWidth: 5,
        borderRightWidth: 5,
        borderTopWidth: 10,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: '#00BCD4',
    },
    buttonstyle: {
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        marginTop: 10,
        width: 200,
        borderWidth: 2,
        borderRadius: 50,
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        // backgroundColor:"#ffdc4a"
        tintColor: '#ffdc4a',
    },
    buttonstyle2: {
        // borderTopLeftRadius: 10,
        // borderBottomLeftRadius: 10,
        // marginTop: 10,
        marginHorizontal: 100,
        width: 200,
        // borderWidth: 2,
        borderRadius: 50,
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        // backgroundColor:"#ffdc4a",
        tintColor: '#ffdc4a',
        alignSelf: 'center',
    },
})
