import React from 'react'
import { Text, View, Image, TouchableOpacity, FlatList } from 'react-native'
import { windowHeight } from '../../../Util/Dimensions'
import Ant from 'react-native-vector-icons/AntDesign'
import { blue } from '../../Constant/Color'
import Fn from 'react-native-vector-icons/FontAwesome5'
import Button from '../../Global/Button'
import { useState } from 'react'

const cardata = [
    {
        icon: 'car-alt',
        name: 'Car',
        id: 0,

    }, {
        icon: 'train',
        name: 'Train',
        id: 1,
    }, {
        icon: 'bus-alt',
        name: 'Car',
        id: 2,
    }
    , {
        icon: 'train',
        name: 'Train',
        id: 3,
    }


]

export default function OnBoardingsilide3({ HandleBoarding, navigation }) {

    const [select, setselect] = useState(0)
    return (
        <View style={{ backgroundColor: 'white', margin: 20, borderRadius: 20, padding: 20 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 30, fontWeight: 'bold', marginTop: 10, marginLeft: 20 }}>Choose your ride</Text>
                <Text style={{ fontSize: 30, color: 'green', marginRight: 20, marginTop: 10 }}> ₹254</Text>
            </View>
            <Text style={{ fontSize: 16, marginLeft: 20, marginBottom: 0 }}>12 min left</Text>


            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={cardata}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                    <TouchableOpacity style={{
                        marginLeft: 20, marginTop: 10, alignItems: 'center',
                        borderWidth: select === item.id ? 1 : null, padding: 10, height: 100
                    }} onPress={() => setselect(item.id)}>

                        <Fn style={{ padding: 10, borderRadius: 10, backgroundColor: 'black', alignSelf: 'center' }}
                            name={item.icon} size={25} color='white' />
                        <Text style={{ marginTop: 5, alignSelf: 'center' }}>{item.name}</Text>

                    </TouchableOpacity>

                )}
            />

            <View style={{ marginTop: 10 }}>
                <Button txt="Book" HandleBoarding={HandleBoarding} />
            </View>
        </View>
    )
}
