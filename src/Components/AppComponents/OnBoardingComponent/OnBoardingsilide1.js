import React from 'react';
import {Text, View, Image} from 'react-native';
import {windowHeight} from '../../../Util/Dimensions';
import Ant from 'react-native-vector-icons/AntDesign';
import {blue} from '../../Constant/Color';
import Button from '../../Global/Button';
import ongoing from '../../../assets/image/ongoing1.png';
import car from '../../../assets/image/car.png';
import {roundToNearestPixel} from 'react-native/Libraries/Utilities/PixelRatio';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';

export default function OnBoardingsilide1({
  HandleLeft,
  oboarding1,
  navigation,
  backScreen,
}) {
  const HandleBoarding1 = () => {
    navigation.navigate('OnGetting', {backScreen: backScreen});
  };

  const handleExplore = () => {
    navigation.navigate('ExploreTabs', {backScreen: backScreen});
  };

  return (
    <View
      style={{
        backgroundColor: 'white',
        // margin: 20,
        borderRadius: 20,
        padding: 10,
      }}>
      <View>
        <Text
          style={{
            fontSize: responsiveFontSize(3),
            fontWeight: 'bold',
            marginTop: 20,
            marginLeft: 20,
          }}>
          Welcome
        </Text>
        <Text
          style={{
            marginTop: 10,
            marginLeft: 20,
            color: '#565656',
            fontSize: 16,
          }}>
          Let's choose your role.
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          marginTop: 20,
          alignItems: 'center',
        }}>
        <Ant name="left" size={30} color={blue} onPress={() => HandleLeft()} />

        {oboarding1 ? (
          <Image
            source={ongoing}
            resizeMode="stretch"
            style={{width: responsiveWidth(50), height: responsiveHeight(20)}}
          />
        ) : (
          <Image
            source={car}
            resizeMode="stretch"
            style={{width: responsiveWidth(50), height: responsiveHeight(20)}}
          />
        )}

        <Ant name="right" size={30} color={blue} onPress={() => HandleLeft()} />
      </View>
      <Text
        style={{
          alignSelf: 'center',
          fontSize: responsiveFontSize(2),
          marginTop: 10,
        }}>
        {oboarding1 ? 'I feel like Driving' : 'I feel like Riding'}
      </Text>
      <View style={{marginTop: 5}}>
        <Button
          txt="Choose"
          oboarding1={oboarding1}
          HandleBoarding={HandleBoarding1}
          blueButton={true}
        />
      </View>
      <View style={{marginBottom: 30}}>
        <Button
          txt="Explore"
          oboarding1="Explore"
          HandleBoarding={handleExplore}
          blueButton={false}
        />
      </View>
    </View>
  );
}
