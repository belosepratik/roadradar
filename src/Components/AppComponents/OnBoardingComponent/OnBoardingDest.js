import React from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  Pressable,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {windowHeight} from '../../../Util/Dimensions';

import {blue} from '../../Constant/Color';
import Button from '../../Global/Button';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {MAP_KEY} from '../../../../Config';
import {useSelector} from 'react-redux';
import MyLocation from '../../Global/MyLocation';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Backarrow from 'react-native-vector-icons/Ionicons';
import Wrapped from '../../Global/Wrapped';
import {BackButton} from '../../Universal/BackButton';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';

export default function OnBoardingDest({navigation, route, HandleBoarding}) {
  const home = useSelector((state) => state.home);
  const {myLocation, destlatlong, flow} = home;

  return (
    <Wrapped>
      <BackButton navigation={navigation} route={route} />
      <MapView
        style={{flex: 1}}
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 19.2068505,
          longitude: 72.9528205,
          latitudeDelta: 0.0222,
          longitudeDelta: 0.0121,
        }}></MapView>

      <View
        style={{flex: 1, flexDirection: 'column', backgroundColor: '#ACAAAA'}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            padding: 10,
          }}>
          <MyLocation />
        </View>
        <View
          style={{
            backgroundColor: 'white',
            margin: 5,
            flex: 1,
            flexDirection: 'column',
            borderRadius: 15,
            padding: 10,
          }}>
          <Text
            style={{
              fontSize: responsiveFontSize(3),
              fontWeight: 'bold',
              marginTop: 5,
              marginLeft: 20,
            }}>
            Enter Your Destination
          </Text>
          <Text
            // style={{marginTop: 5, marginLeft: 20, marginBottom: 20}}
            style={{marginLeft: 20, marginTop: 5, color: '#565656'}}>
            Please tell us your drop location
          </Text>

          <View
            style={{
              // justifyContent: 'space-evenly',
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <View
              style={{flexDirection: 'column', marginLeft: 10, marginTop: 20}}>
              <Image
                // style={styles.tinyLogo}
                source={require('../../../assets/image/startloc.png')}
                style={{
                  height: responsiveHeight(2.6),
                  width: responsiveWidth(4.5),
                }}
              />
              <Image
                // style={styles.tinyLogo}
                source={require('../../../assets/image/connector.png')}
                style={{
                  height: responsiveHeight(5),
                  marginLeft: 7,
                  width: responsiveWidth(1),
                }}
              />
              <Image
                // style={styles.tinyLogo}
                source={require('../../../assets/image/endloc.png')}
                style={{
                  height: responsiveHeight(7),
                  width: responsiveWidth(6),
                }}
              />
            </View>
            <View
              style={{
                flexDirection: 'column',
                padding: 10,
                paddingLeft: 20,
              }}>
              <Text
                style={{
                  padding: 10,
                  borderWidth: 0.2,
                  borderRadius: 5,
                  fontSize: responsiveFontSize(2),
                }}>
                {myLocation}
              </Text>
              <TouchableOpacity
                style={{marginTop: 5}}
                onPress={() => HandleBoarding()}>
                <Text
                  style={{
                    padding: 10,
                    borderWidth: 0.2,
                    borderRadius: 5,
                    fontSize: responsiveFontSize(2),
                    color: 'grey',
                    width: 270,
                  }}>
                  Enter Destination
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Wrapped>
  );
}

const styles1 = StyleSheet.create({
  container: {
    // flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    // backgroundColor: 'black',
  },
  searchBox: {
    marginTop: Platform.OS === 'ios' ? 40 : 10,
    flexDirection: 'row',
    // backgroundColor: '#fff',
    // width: '90%',
    // alignSelf:'center',
    borderRadius: 5,
    paddingVertical: 20,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: responsiveHeight(50)},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    // elevation: 10,
  },
  ham: {
    marginTop: Platform.OS === 'ios' ? 40 : 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    // width: '90%',
    // alignSelf:'center',
    borderRadius: 5,
    padding: 10,
    margin: 10,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: responsiveHeight(50)},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  searchBtn: {
    marginTop: Platform.OS === 'ios' ? 200 : 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 15,
    padding: 10,
    shadowColor: '#ccc',
    shadowOffset: {width: responsiveWidth(50), height: responsiveHeight(50)},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  autoCompleteContainer: {
    top: 22,
    left: 10,
    right: 10,
  },
  textInput: {
    padding: 10,
    width: '90%',
    // backgroundColor: '#eee',
    color: 'gray',
    marginVertical: 5,
    marginLeft: 20,
  },
  circle: {
    width: responsiveWidth(50),
    height: responsiveHeight(50),
    backgroundColor: '#ffdc4a',
    position: 'absolute',
    top: 20,
    left: 5,
    borderRadius: 5,
  },
  line: {
    width: 1,
    height: responsiveHeight(50),
    backgroundColor: '#c4c4c4',
    position: 'absolute',
    top: 40,
    left: 10,
    borderRadius: 5,
  },
  square: {
    top: -20,
    left: 5,
    width: 0,
    height: responsiveHeight(50),
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderTopWidth: 10,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#00BCD4',
  },
  buttonstyle: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    marginTop: 10,
    width: 200,
    borderWidth: 2,
    borderRadius: 50,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    // backgroundColor:"#ffdc4a"
    tintColor: '#ffdc4a',
  },
  buttonstyle2: {
    // borderTopLeftRadius: 10,
    // borderBottomLeftRadius: 10,
    // marginTop: 10,
    marginHorizontal: 100,
    width: responsiveWidth(50),
    // borderWidth: 2,
    borderRadius: 50,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    // backgroundColor:"#ffdc4a",
    tintColor: '#ffdc4a',
    alignSelf: 'center',
  },
});
