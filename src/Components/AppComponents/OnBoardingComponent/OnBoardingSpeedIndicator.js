import React from 'react'
import { Text, View, Image, StyleSheet, Pressable, TextInput, TouchableOpacity } from 'react-native'
import { windowHeight } from '../../../Util/Dimensions'
import Ant from 'react-native-vector-icons/AntDesign'
import MT from 'react-native-vector-icons/MaterialCommunityIcons'
import Octicons from 'react-native-vector-icons/Octicons'
import { blue } from '../../Constant/Color'
import Button from '../../Global/Button'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import { MAP_KEY } from '../../../../Config'
import { useSelector } from 'react-redux'
import MyLocation from '../../Global/MyLocation'
import GlobalBoxIcon from '../../Global/GlobalBoxIcon'
import RNSpeedometer from 'react-native-speedometer'
export default function OnBoardingSpeedIndicator({ HandleBoarding4 }) {
    return (
        <View style={{ flex: 1, justifyContent: 'space-between' }}>

            <View style={{ padding: 10, margin: 10, borderRadius: 20 }} />

            <View style={{ alignItems: 'flex-end', marginBottom: 20 }}>
                <GlobalBoxIcon name='plus' />
                <View style={{ marginTop: 20 }}>
                    <GlobalBoxIcon name='minus' />
                </View>
            </View>



            <View>
                <TouchableOpacity onPress={() => HandleBoarding4(3)} style={{ alignItems: 'flex-end', marginBottom: 20 }}>
                    <MyLocation />
                </TouchableOpacity>
                <TouchableOpacity style={{ backgroundColor: 'white', padding: 0, margin: 10, borderRadius: 20, marginBottom: 40 }}

                >

                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', padding: 20, alignContent: 'center' }}>
                        <View style={{ padding: 20, top: -20 }}>
                            <RNSpeedometer value={50} size={50} />
                        </View>
                        <View style={{ padding: 20, top: -20 }}>
                            <RNSpeedometer value={80} size={50} />
                        </View>

                    </View>

                </TouchableOpacity>
            </View>

        </View>
    )
}
