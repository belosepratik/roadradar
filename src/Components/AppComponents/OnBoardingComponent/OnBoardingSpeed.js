import React from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  Pressable,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
// import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

import {windowHeight} from '../../../Util/Dimensions';
import Ant from 'react-native-vector-icons/AntDesign';
import MT from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import {blue} from '../../Constant/Color';
import Button from '../../Global/Button';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {MAP_KEY} from '../../../../Config';
import {useSelector} from 'react-redux';
import MyLocation from '../../Global/MyLocation';
import GlobalBoxIcon from '../../Global/GlobalBoxIcon';
import Navigation from '../../../Routes/Navigation';
import Wrapped from '../../Global/Wrapped';
import {FloatingIcons} from '../../Universal/FloatingIcons';

import {carpool1} from '../../../assets/constants/images';

import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';

export default function OnBoardingSpeed({navigation, route}) {
  console.log('CarpoolPage Current >>>>>>>>>>>', route.name);
  console.log('CarpoolPage Prev >>>>>>>>>>>', route.params.backScreen);

  const OnBoardingSpeedHandle = () => {
    navigation.navigate('ExploreTabs', {backScreen: route.name});
  };

  const OnSpeed = () => {
    navigation.navigate('Speed', {backScreen: route.name});
  };

  return (
    // <Wrapped>
    //   <MapView
    //     style={{flex: 1}}
    //     showsUserLocation={true}
    //     provider={PROVIDER_GOOGLE}
    //     initialRegion={{
    //       latitude: 19.2068505,
    //       longitude: 72.9528205,
    //       latitudeDelta: 0.0222,
    //       longitudeDelta: 0.0121,
    //     }}></MapView>
    <>
      <View style={{flex: 1, justifyContent: 'space-between'}}>
        <View
          style={{
            backgroundColor: 'white',
            padding: 10,
            margin: 10,
            borderRadius: 20,
          }}>
          <View style={{alignItems: 'flex-end', marginRight: 5, marginTop: 5}}>
            <Ant name="close" size={15} color="#000000" />
          </View>

          <TouchableOpacity>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <View style={{alignItems: 'center'}}>
                <View>
                  <Image
                    // style={styles.tinyLogo}
                    source={require('../../../assets/image/carpool1.png')}
                    // style={
                    //   {
                    //     height: responsiveHeight(7),
                    //     width: responsiveWidth(10),
                    //   }
                    // }
                  />
                </View>
                <View style={{alignItems: 'center'}}>
                  <Text style={{fontSize: responsiveFontSize(1.8)}}>
                    Carpool
                  </Text>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: responsiveFontSize(2),
                    }}>
                    150m
                  </Text>
                </View>
              </View>
              <View style={{alignItems: 'center'}}>
                <Image
                  // style={styles.tinyLogo}
                  source={require('../../../assets/image/pothole.png')}
                  style={
                    {
                      //height: responsiveHeight(7),
                      // width: responsiveWidth(10),
                    }
                  }
                />
                <Text
                  style={{fontSize: responsiveFontSize(1.8), marginTop: 12}}>
                  Pothhole
                </Text>
                <Text
                  style={{fontWeight: 'bold', fontSize: responsiveFontSize(2)}}>
                  200m
                </Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <Image
                  // style={styles.tinyLogo}
                  source={require('../../../assets/image/t-icon.png')}
                  // style={{
                  //   height: responsiveHeight(7),
                  //   width: responsiveWidth(13),
                  // }}
                />
                <Text style={{fontSize: responsiveFontSize(1.8)}}>
                  Traffic Signal
                </Text>
                <Text
                  style={{fontWeight: 'bold', fontSize: responsiveFontSize(2)}}>
                  500m
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
        <FloatingIcons
          bottomPosition="24%"
          navigation={navigation}
          onHandle={OnSpeed}
          icon="speed"
        />
        <FloatingIcons
          bottomPosition="35%"
          navigation={navigation}
          onHandle={OnBoardingSpeedHandle}
          icon="globe"
        />
        <MapView
          style={{flex: 1}}
          showsUserLocation={true}
          provider={PROVIDER_GOOGLE}
          initialRegion={{
            latitude: 19.2068505,
            longitude: 72.9528205,
            latitudeDelta: 0.0222,
            longitudeDelta: 0.0121,
          }}></MapView>
        {/* <View
          style={{
            flex: 0.2,
            // justifyContent: 'flex-end',
          }}>
          <Image
            // style={styles.tinyLogo}
            source={require('../../../assets/image/explore.png')}
            style={{
              height: 300,
              width: 300,
            }}
          />
          <Image
            // style={styles.tinyLogo}
            source={require('../../../assets/image/speedcheck.png')}
            style={{
              height: 300,
              width: 300,
            }}
          />
        </View> */}
        <View>
          <TouchableOpacity
            style={{
              backgroundColor: 'white',
              padding: 15,
              margin: 10,
              borderRadius: 20,
              marginBottom: 40,
              //height: '50%',
            }}
            onPress={() =>
              navigation.navigate('Explore', {backScreen: route.name})
            }>
            <View
              style={{
                alignItems: 'flex-end',
                marginRight: 10,
                marginTop: 10,
              }}></View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              <View style={{alignItems: 'center'}}>
                <Text
                  style={{fontSize: responsiveFontSize(2), color: '#565656'}}>
                  PICKED UP
                </Text>
                <Text
                  style={{fontSize: responsiveFontSize(2), fontWeight: 'bold'}}>
                  0/3
                </Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <Text
                  style={{fontSize: responsiveFontSize(2), color: '#565656'}}>
                  TIME LEFT
                </Text>
                <Text
                  style={{fontSize: responsiveFontSize(2), fontWeight: 'bold'}}>
                  14
                </Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <Text
                  style={{fontSize: responsiveFontSize(2), color: '#565656'}}>
                  DISTANCE
                </Text>
                <Text
                  style={{fontSize: responsiveFontSize(2), fontWeight: 'bold'}}>
                  2.3Km
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>

      {/* <View
        style={{
          width: 70,
          marginTop: windowHeight / 2,
          position: 'absolute',
        }}>
          
        <GlobalBoxIcon name="plus" />
      </View>
      <View style={{width: 70, marginTop: 10}}>
        <GlobalBoxIcon name="minus" />
      </View>
      <View style={{width: 70, marginTop: 10}}>
        <MyLocation />
      </View> */}
    </>
  );
}
