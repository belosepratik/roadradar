import React, { useState } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { windowHeight } from '../../../Util/Dimensions'
import Ant from 'react-native-vector-icons/AntDesign'
import { blue } from '../../Constant/Color'
import Fn from 'react-native-vector-icons/FontAwesome5'
import Button from '../../Global/Button'
import MyLocation from '../../Global/MyLocation'
export default function OnBoardingsilide2({ HandleBoarding }) {
    const [select, setselect] = useState(0)
    return (
        <View>
            <View style={{ alignItems: 'flex-end', marginBottom: 20 }}>
                <MyLocation />
            </View>
            <View style={{ backgroundColor: 'white', margin: 20, borderRadius: 20, padding: 10 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 30, fontWeight: 'bold', marginTop: 20, marginLeft: 20 }}>Let's Go</Text>
                    <TouchableOpacity style={{ marginRight: 20, marginTop: 20 }} onPress={() => HandleBoarding(2)}>
                        <Ant style={{ backgroundColor: blue, padding: 10, alignSelf: 'center', borderRadius: 10 }} name="arrowright" size={30} color='white' /></TouchableOpacity>
                </View>
                <Text style={{ fontSize: 16, marginLeft: 20, marginTop: -10 }}>12 min left</Text>
                <TouchableOpacity onPress={() => setselect(0)} style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, alignItems: 'center', borderWidth: select == 0 ? 1 : null, padding: 5, borderRadius: 10 }}>
                    <Fn style={{ padding: 10, borderRadius: 10, backgroundColor: 'yellow', alignSelf: 'center' }} name='parking' size={25} color='black' />
                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Parking</Text>
                        <Text>Available /1h 30mins</Text>
                    </View>

                </TouchableOpacity>

                <TouchableOpacity onPress={() => setselect(1)} style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10, alignItems: 'center', borderWidth: select == 1 ? 1 : null, padding: 5, borderRadius: 10 }}>

                    <Fn style={{ padding: 10, borderRadius: 10, backgroundColor: '#fd4b5d', alignSelf: 'center' }} name='car-alt' size={25} color='black' />

                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Carpool</Text>
                        <Text>Available /1h 30mins</Text>
                    </View>

                </TouchableOpacity>
            </View>
        </View>

    )
}
