import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import React from 'react';
import {
  Text,
  View,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Button,
  TouchableHighlight,
  SafeAreaView,
  Animated,
  } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import FeatherIcon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import ProfileBox from './ProfileBox';
import SettingsList from './SettingList';

const ModalPoup = ({visible, children}) => {
  const [showModal, setShowModal] = React.useState(visible);
  const scaleValue = React.useRef(new Animated.Value(0)).current;
  React.useEffect(() => {
    toggleModal();
  }, [visible]);
  const toggleModal = () => {
    if (visible) {
      setShowModal(true);
      Animated.spring(scaleValue, {
        toValue: 1,
        duration: 300,
        useNativeDriver: true,
      }).start();
    } else {
      setTimeout(() => setShowModal(false), 200);
      Animated.timing(scaleValue, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true,
      }).start();
    }
  };
  return (
    <Modal transparent visible={showModal}>
      <View style={styles.modalBackGround}>
        <Animated.View
          style={[styles.modalContainer, {transform: [{scale: scaleValue}]}]}>
          {children}
        </Animated.View>
      </View>
    </Modal>
  );
};

export default function ProfileScreen({navigation}) {
  const [visible, setVisible] = React.useState(false);
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        <View
          style={{
            backgroundColor: '#f5f7ff',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              margin: 20,
            }}>
            <Text style={{fontSize: 24, fontWeight: 'bold'}}>Profile</Text>
            <Text style={{fontSize: 30, fontWeight: 'bold'}}>...</Text>
          </View>

          <ProfileBox
            fname="John"
            lname="Doe"
            law="Student"
            address="Maharashtra Mumbai"
            email="abc@gmail.com"
            phone="+91-9856231452"
          />

          <View
            style={{
              margin:20,
              borderRadius:10,
              backgroundColor: 'white',
              opacity: 1,
              marginBottom:100,
              shadowcolor: '#000',
              elevation: 10,
              marginTop: 20,
            }}>
            <View
              style={{marginHorizontal: 40, marginBottom: 15, marginTop: 30}}>
              <Text style={{fontSize: 24, fontWeight: 'bold'}}>Settings</Text>
            </View>

            <SettingsList
              iconName="email-outline"
              title="Notification and Description"
            />

            <SettingsList iconName="security" title="Privacy and Security" />

            <SettingsList iconName="database" title="Data Storage" />

            <SettingsList iconName="cog" title="Advance Setting" />

            <SettingsList iconName="devices" title="Devices" />
          </View>
        </View>
      
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  forgotButton: {
    marginVertical: 10,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#2e64e5',
    fontFamily: 'Lato-Regular',
  },
  statsContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 15,
  },
  statsBox: {
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#f0fff0',
    margin: 3,
    padding: 10,
    borderRadius: 6,
  },
  text: {
    fontFamily: 'HelveticaNeue',
  },
  userInfoSection: {
    paddingHorizontal: 10,
    marginBottom: 25,
    marginTop: 30,
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  input: {
    height: 40,
    width: 350,
    margin: 12,
    borderWidth: 1,
  },
});