import React from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  ScrollView,
  View,
  TextInput,
  Modal,
  Button,
  Animated,
  Image,
  TouchableOpacity,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

const ModalPoup = ({visible, children}) => {
  const [showModal, setShowModal] = React.useState(visible);
  const scaleValue = React.useRef(new Animated.Value(0)).current;
  React.useEffect(() => {
    toggleModal();
  }, [visible]);
  const toggleModal = () => {
    if (visible) {
      setShowModal(true);
      Animated.spring(scaleValue, {
        toValue: 1,
        duration: 300,
        useNativeDriver: true,
      }).start();
    } else {
      setTimeout(() => setShowModal(false), 200);
      Animated.timing(scaleValue, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true,
      }).start();
    }
  };
  return (
    <Modal transparent visible={showModal}>
      <View style={styles.modalBackGround}>
        <Animated.View
          style={[styles.modalContainer, {transform: [{scale: scaleValue}]}]}>
          {children}
        </Animated.View>
      </View>
    </Modal>
  );
};

export default function ProfileBox(props) {
  const {
    fname,
    lname,
    law,
    address,
    email,
    phone,
    brand,
    reg_num,
    model,
    make,
    color,
    passengers,
    type,
    model_type,
  } = props;

  const [modal, setModal] = React.useState(false);

  const [visible, setVisible] = React.useState(false);
  return (
    <SafeAreaView>
      <View style={styles.card}>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.img}>
            <Image
              style={{
                width: 100,
                height: 100,
                borderRadius: 25,
                borderWidth: 1,
                resizeMode: 'cover',
                backgroundColor: 'white',
                borderColor: 'grey',
                padding: 10,
              }}
              source={{uri: 'https://picsum.photos/200/300'}}
            />
          </View>
          <View style={{padding: 20, marginTop: 10}}>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>{fname}</Text>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>{lname}</Text>

            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: 'grey',
                marginTop: 10,
              }}>
              {law}
            </Text>
          </View>
          <TouchableOpacity
            style={{marginTop: 32}}
            onPress={() => setVisible(true)}>
            <MaterialCommunityIcons
              name="circle-edit-outline"
              size={24}
              color="grey"
              style={{paddingLeft: 20}}
            />
          </TouchableOpacity>
        </View>
        {address && (
          <View style={{paddingHorizontal: 50, marginTop: 20, width: '100%'}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="map-marker-radius"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{address}</Text>
            </View>
          </View>
        )}

        {email && (
          <View style={{paddingHorizontal: 50}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="email-outline"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{email}</Text>
            </View>
          </View>
        )}

        {brand && (
          <View style={{paddingHorizontal: 50}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="email-outline"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{brand}</Text>
            </View>
          </View>
        )}

        {reg_num && (
          <View style={{paddingHorizontal: 50,marginTop:20,width: '100%'}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="car-estate"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{reg_num}</Text>
            </View>
          </View>
        )}

        {make && (
          <View style={{paddingHorizontal: 50}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="car"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{make}</Text>
            </View>
          </View>
        )}

        {passengers && (
          <View style={{paddingHorizontal: 50}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="seat-passenger"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                {passengers}
              </Text>
            </View>
          </View>
        )}

        {type && (
          <View style={{paddingHorizontal: 50}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="fuel"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{type}</Text>
            </View>
          </View>
        )}

        {model_type && (
          <View style={{paddingHorizontal: 50}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="email-outline"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                {model_type}
              </Text>
            </View>
          </View>
        )}

        {color && (
          <View style={{paddingHorizontal: 50}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="palette"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{color}</Text>
            </View>
          </View>
        )}

        {model && (
          <View style={{paddingHorizontal: 50}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="email-outline"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{model}</Text>
            </View>
          </View>
        )}
        {phone && (
          <View style={{paddingHorizontal: 50, marginBottom: 10}}>
            <View style={styles.info}>
              <MaterialCommunityIcons
                name="phone-in-talk-outline"
                size={24}
                color="grey"
                style={{paddingRight: 10}}
              />
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{phone}</Text>
            </View>
          </View>
        )}
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ModalPoup visible={visible}>
          <View style={{alignItems: 'center'}}>
            <Text style={{fontSize: 20}}>Edit Profile</Text>
            <View style={styles.header}>
              <TouchableOpacity onPress={() => setVisible(false)}>
                <FontAwesome5Icon name="times" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{alignItems: 'center'}}>
            {/* <Image
              source={require('./assets/success.png')}
              style={{height: 150, width: 150, marginVertical: 10}}
            /> */}
            <Text>Image</Text>
          </View>

          <TextInput style={{marginTop: 30, ...styles.txtInputStyle}}>
            Jonathan Deaz
          </TextInput>
          <TextInput style={styles.txtInputStyle}>Lawyer</TextInput>
          <TextInput style={styles.txtInputStyle}>
            Mumbai, Maharashtra
          </TextInput>
          <TextInput style={styles.txtInputStyle}>jonathan@mail.com</TextInput>
          <TextInput style={styles.txtInputStyle}>+91-9876543210</TextInput>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 50,
              justifyContent: 'space-between',
            }}>
            <Text style={{color: '#ffdc4a', fontSize: 15, marginTop: 10}}>
              Change Password?
            </Text>
            <TouchableOpacity
              onPress={() => setVisible(false)}
              style={{
                backgroundColor: '#ffdc4a',
                borderRadius: 25,
                paddingHorizontal: 40,
                paddingVertical: 10,
              }}>
              <Text style={{color: 'white'}}>Save</Text>
            </TouchableOpacity>
          </View>
        </ModalPoup>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    fontSize: 42,
  },
  card: {
    padding: 5,
    margin: 10,
    borderRadius: 10,
    elevation: 15,
    backgroundColor: '#fff',
    marginHorizontal: 20,
  },
  img: {
    backgroundColor: 'white',
    borderRadius: 25,
    padding: 10,
    borderColor: 'rgb(37, 150, 190)',
    borderWidth: 3,
    marginTop: 20,
    marginLeft: 20,
  },
  info: {
    flexDirection: 'row',
    padding: 10,
  },
  modalBackGround: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    width: '80%',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
    elevation: 20,
    height: '80%',
  },
  header: {
    width: '100%',
    height: 40,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  txtInputStyle: {
    fontSize: 20,
    borderBottomWidth: 1,
    borderColor: '#ffdc4a',
    color: 'gray',
  },
});
