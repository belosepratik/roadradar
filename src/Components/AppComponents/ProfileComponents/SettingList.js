import React from 'react';
import {Text, View} from 'react-native';
import  MaterialCommunityIcons  from "react-native-vector-icons/MaterialCommunityIcons";

export default function SettingsList(props) {
  const {iconName, title} = props;

  return (
    <View style={{marginHorizontal: 20,}}>
      <View style={{flexDirection: 'row', padding: 10}}>
        <MaterialCommunityIcons
          name={iconName}
          size={24}
          color="gray"
          style={{ paddingRight: 10 }}
        />
        <Text style={{fontSize: 18, fontWeight: 'bold'}}>{title}</Text>
      </View>
    </View>
  );
}