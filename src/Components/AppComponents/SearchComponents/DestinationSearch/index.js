import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, SafeAreaView, StyleSheet } from 'react-native';
import PlaceRow from './PlaceRow';
import styles from './styles';
import Geolocation from '@react-native-community/geolocation';
import { MAP_KEY } from '../../../../../Config';
import { useSelector } from 'react-redux';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Input from '../../../Basic/Input';


const DestinationSearch = () => {
  const home = useSelector(state => state.home)
  const { myLocation } = home
  let key = {
    start: 0,
    end: 0
  }

  return (

    <>
      <View style={{marginLeft:30,backgroundColor:'white',marginTop:10}}>
        <TextInput
          style={{height:40}}
          placeholder="From"
          // placeholderTextColor="gray"
          value={myLocation}
          selection={key}
          // onChangeText={e=>console.log(e)}
        />
      </View>

      <View style={{marginTop:5}}>
        <GooglePlacesAutocomplete
          styles={{
            textInputContainer: {
              marginLeft: 30,
              marginTop: 0,

            },
            textInput: {
              borderRadius: 0,
              color: 'grey'
            },
          }}
          placeholder='Search'
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true
            console.log(data, details);
          }}
          query={{
            key: MAP_KEY,
            language: 'en',
          }}
        />
      </View>


      <View style={styles1.circle}></View>

      {/* Line between dots */}
      <View style={styles1.line}></View>

      {/* Square near Destination Input */}
      <View style={styles1.square}></View>
    </>

  );
};

export default DestinationSearch;

const styles1 = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    // backgroundColor: 'black',
  },
  searchBox: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 40 : 10,
    flexDirection: 'row',
    // backgroundColor: '#fff',
    // width: '90%',
    // alignSelf:'center',
    borderRadius: 5,
    paddingVertical: 20,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    // elevation: 10,
  },
  ham: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 40 : 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    // width: '90%',
    // alignSelf:'center',
    borderRadius: 5,
    padding: 10,
    margin: 10,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  searchBtn: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 200 : 120,
    flexDirection: 'row',
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 15,
    padding: 10,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  autoCompleteContainer: {
    position: 'absolute',
    top: 22,
    left: 10,
    right: 10,
  },
  textInput: {
    padding: 10,
    width: '90%',
    // backgroundColor: '#eee',
    color: 'gray',
    marginVertical: 5,
    marginLeft: 20,
  },
  circle: {
    width: 10,
    height: 10,
    backgroundColor: '#ffdc4a',
    position: 'absolute',
    top: 20,
    left: 5,
    borderRadius: 5,
  },
  line: {
    width: 1,
    height: 45,
    backgroundColor: '#c4c4c4',
    position: 'absolute',
    top: 40,
    left: 10,
    borderRadius: 5,
  },
  square: {
    top: -20,
    left: 5,
    width: 0,
    height: 0,
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderTopWidth: 10,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#00BCD4',
  },
  buttonstyle: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    marginTop: 10,
    width: 200,
    borderWidth: 2,
    borderRadius: 50,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    // backgroundColor:"#ffdc4a"
    tintColor: '#ffdc4a',
  },
  buttonstyle2: {
    // borderTopLeftRadius: 10,
    // borderBottomLeftRadius: 10,
    // marginTop: 10,
    marginHorizontal: 100,
    width: 200,
    // borderWidth: 2,
    borderRadius: 50,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    // backgroundColor:"#ffdc4a",
    tintColor: '#ffdc4a',
    alignSelf: 'center',
  },
})
