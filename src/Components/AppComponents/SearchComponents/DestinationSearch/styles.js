import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  textInput: {
    padding: 10,
    backgroundColor: '#eee',
    marginVertical: 5,
    marginLeft:20,
    color:"gray"

  },
  container: {
    padding: 10,
    //    backgroundColor:"#eee",
    height: '100%',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  iconContainer: {
    backgroundColor: '#a2a2a2',
    padding:5,
    borderRadius:50,
    marginRight:15,
  },
  locationText: {
    color:"black",
  },
  separator:{
   backgroundColor:"#efefef",
   height:1,
 },
 listView:{
   position:'absolute',
   top:105,
 },
 autoCompleteContainer:{
   position:'absolute',
   top:22,
   left:10,
   right:10
 },
 circle:{
   width:10,
   height:10,
   backgroundColor:"#ffdc4a",
   position:"absolute",
   top:35,
   left:15,
   borderRadius:5
 },
 line:{
   width:1,
   height:45,
   backgroundColor:"#ffdc4a",
   position:"absolute",
   top:50,
   left:20,
   borderRadius:5
 },
 square:{
    top:90,
    left:5,
    width: 0,
    height: 0,
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderTopWidth:10,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#00BCD4'
 },
 

});

export default styles;
