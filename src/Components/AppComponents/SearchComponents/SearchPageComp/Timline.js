import React, { createRef, useState } from 'react'
import {
    Image,
    View,
    SafeAreaView,
    ImageBackground,
    Dimensions,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import Timeline from 'react-native-timeline-flatlist';

import SearchActionSheet from '../../../Actionsheet/SearchActionSheet';
import { windowHeight } from '../../../../Util/Dimensions';
export default function Timline({ data, SelectRoute, navigation, HandleRoute }) {


    const renderDetail = (rowData, sectionID, rowID) => {
        let title = <Text style={{ fontSize: 20, marginLeft: 10 }}>{rowData.title}</Text>
        var desc = null
        if (rowData.description)
            desc = (
                <View>
                    {/* <Image source={{uri: rowData.imageUrl}} style={styles.image}/> */}
                    <Text >{rowData.description}</Text>
                </View>
            )

        return (
            <View>
                {title}
                {rowData.button ?
                    <>
                        <View >
                            <FlatList
                                data={SelectRoute}
                                numColumns={2}
                                contentContainerStyle={{ marginTop: 20 }}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }) => (
                                    <TouchableOpacity onPress={() => {
                                        // console.log(item.name)
                                        HandleRoute(item.name)
                                        

                                    }}
                                        style={{ width: '50%',marginLeft: 10, marginTop: 10 }} >
                                        <Text style={{ padding: 15,height:50, backgroundColor: '#ffdc4a' }}>{item.name}</Text>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    </>
                    : desc
                }

            </View>
        )
    }
    return (
        <View style={{ marginTop: 10, marginHorizontal: 15 }}>
            <Timeline
                data={data}
                circleSize={20}
                circleColor="#ffdc4a"
                lineColor="#ffdc4a"
                innerCircle={'icon'}
                // timeContainerStyle={{minWidth:52, marginTop: -5}}
                timeStyle={{ textAlign: 'center', backgroundColor: 'black', color: '#ffdc4a', padding: 5, margin: 0, borderRadius: 13 }}
                // descriptionStyle={{ color: 'gray' }}
                // detailContainerStyle={{ marginBottom: 20, paddingLeft: 5, paddingRight: 5, backgroundColor: "#BBDAFF", borderRadius: 10 }}
                // // columnFormat='single-column-left'
                // onEventPress={() => alert(data[0].title,)}
                renderDetail={renderDetail}
                options={{
                    style: {}
                }}
            />
        </View>
    )
}
