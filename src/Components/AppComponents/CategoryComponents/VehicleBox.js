import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {windowHeight, windowWidth} from '../../../Util/Dimensions';
// import HeartIcon from '../../Global/HeartIcon'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {deleteDataById, getAllData} from '../../../Axios/axios';
import {useDispatch} from 'react-redux';

export default function VehicleBox(props) {
  const dispatch = useDispatch();
  const {vehicleData} = props;

  const FootTab = [
    {
      id: 1,
      reg_num: 'MH042899',
      brand: 'Hyundai',
      model: 'i20',
      make: 'magna',
      color: 'maroon',
      passengers: '3',
      type: 'diesel',
      model_type: 'hatchback',
    },
    {
      id: 2,
      reg_num: 'MH042899',
      brand: 'Hyundai',
      model: 'i20',
      make: 'magna',
      color: 'maroon',
      passengers: '3',
      type: 'diesel',
      model_type: 'hatchback',
    },
    {
      id: 3,
      reg_num: 'MH042899',
      brand: 'Maruti Suzuki AayaBhosada',
      model: 'i20',
      make: 'magna',
      color: 'maroon',
      passengers: '3',
      type: 'diesel',
      model_type: 'hatchback',
    },
  ];

  return (
    <View>
      <View>
        <FlatList
          data={vehicleData}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{paddingTop: 20}}
          renderItem={({item}) => (
            <>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  margin: 10,
                  padding: 5,
                  elevation: 15,
                  backgroundColor: 'white',
                  marginHorizontal: 20,
                  borderRadius: 10,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    source={require('../../../assets/lambo.jpg')}
                    style={{height: 100, width: 100, borderRadius: 10}}
                  />

                  <View style={{marginLeft: 20}}>
                    {/* <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                      {item.brand.length < 10
                        ? `${item.brand} ${
                            item.model.length < 7
                              ? `${item.model}`
                              : `${item.model.substring(0, 7)}..`
                          }`
                        : `${item.brand.substring(0, 10)}... ${
                            item.model.length < 5
                              ? `${item.model}`
                              : `${item.model.substring(0, 5)}..`
                          }`}
                    </Text> */}
                    <View style={{flexDirection: 'row'}}>
                      <Text style={{color: 'grey', marginTop: 5}}>
                        {item.reg_num}
                      </Text>
                      <Text
                        style={{color: 'grey', marginTop: 5, marginLeft: 10}}>
                        {item.make}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 5,
                        alignItems: 'center',
                      }}>
                      <FontAwesome5Icon name="user-friends" size={15} />
                      {/* <HeartIcon icon="MaterialCommunityIcons" name="percent" size={15} color="white" bg="#f06b32" wh={15} /> */}
                      <Text style={{margin: 4}}>{item.passengers}</Text>
                      <FontAwesome5Icon name="gas-pump" size={15} />
                      <Text style={{color: 'grey', margin: 4}}>
                        {item.type}
                      </Text>
                      <FontAwesome5Icon name="palette" size={15} />
                      <Text style={{color: 'grey', margin: 4}}>
                        {item.color}
                      </Text>
                    </View>
                  </View>
                </View>
                {/* <HeartIcon icon="Ionicons" name="heart-outline" size={25} color="#f06b32" /> */}
                <View style={{paddingRight: 5}}>
                  <TouchableOpacity
                    onPress={() => {
                      dispatch(deleteDataById(item._id, 'vehicle'));
                      
                    }}>
                    <FontAwesome5Icon name="trash-alt" size={18} />
                  </TouchableOpacity>
                </View>
              </View>

              {/* <View style={{borderWidth:0.2,marginTop:10}}/> */}
            </>
          )}
        />
      </View>
    </View>
  );
}
