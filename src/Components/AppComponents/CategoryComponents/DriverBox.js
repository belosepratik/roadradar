import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {windowHeight, windowWidth} from '../../../Util/Dimensions';
// import HeartIcon from '../../Global/HeartIcon'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {deleteDataById} from '../../../Axios/axios';
import {useDispatch} from 'react-redux';



export default function DriverBox(props) {
  const dispatch = useDispatch();
  const {driverData} = props;

  const FootTab = [
    {
      id: 1,
      reg_num: 'MH042899',
      brand: 'Hyundai',
      model: 'i20',
      make: 'magna',
      color: 'maroon',
      passengers: '3',
      type: 'diesel',
      model_type: 'hatchback',
    },
    {
      id: 2,
      reg_num: 'MH042899',
      brand: 'Hyundai',
      model: 'i20',
      make: 'magna',
      color: 'maroon',
      passengers: '3',
      type: 'diesel',
      model_type: 'hatchback',
    },
    {
      id: 3,
      reg_num: 'MH042899',
      brand: 'Maruti Suzuki AayaBhosada',
      model: 'i20',
      make: 'magna',
      color: 'maroon',
      passengers: '3',
      type: 'diesel',
      model_type: 'hatchback',
    },
  ];

  return (
    <View>
      <View>
        <FlatList
          data={driverData}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{paddingTop: 20}}
          renderItem={({item}) => (
            <>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  margin: 10,
                  padding: 5,
                  elevation: 15,
                  backgroundColor: 'white',
                  marginHorizontal: 20,
                  borderRadius: 10,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    source={require('../../../assets/driver.jpg')}
                    style={{height: 100, width: 100, borderRadius: 10}}
                  />

                  <View style={{marginLeft: 20}}>
                    {/* <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                      {item.name.length < 10
                        ? `${item.name} `
                        : `${item.name.substring(0, 10)}...`}
                    </Text> */}
                    <View style={{flexDirection: 'row'}}>
                    <FontAwesome5Icon name="map-marker-alt" size={15} style={{marginTop: 4}} />
                      <Text style={{color: 'grey', margin: 4}}>
                        {item.address}
                      </Text>
                      <FontAwesome5Icon name="phone-alt" size={15} style={{marginTop: 4}} />
                      <Text
                        style={{color: 'grey', margin: 4, marginLeft: 5}}>
                        {item.mob_no}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 5,
                        alignItems: 'center',
                      }}>
                      <FontAwesome5Icon name="car" size={15} />
                      {/* <HeartIcon icon="MaterialCommunityIcons" name="percent" size={15} color="white" bg="#f06b32" wh={15} /> */}
                      <Text style={{margin: 4}}>{item.vehicle_name}</Text>
                      
                    </View>
                  </View>
                </View>
                {/* <HeartIcon icon="Ionicons" name="heart-outline" size={25} color="#f06b32" /> */}
                <View style={{paddingRight: 5}}>
                  <TouchableOpacity onPress={() =>  dispatch(deleteDataById(item._id,'driver')) }>
                    <FontAwesome5Icon name="trash-alt" size={18} />
                  </TouchableOpacity>
                </View>
              </View>

              {/* <View style={{borderWidth:0.2,marginTop:10}}/> */}
            </>
          )}
        />
      </View>
    </View>
  );
}
