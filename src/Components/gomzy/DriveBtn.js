import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useSelector} from 'react-redux';

function DriveBtn(props) {
  const home = useSelector((state) => state.home);
  const {flow} = home;

  const {title, HandleButton} = props;

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        if (flow == 'Schedule Ride') {
          console.log('Flow finished For Shedule Ride');
        } else {
          HandleButton();
        }
      }}>
      <Text style={styles.txt}>{title}</Text>
      <Icon name="trending-flat" style={styles.icn}></Icon>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1d91ca',
    width: vw(75),
    height: vw(15),
    borderRadius: vw(4),
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: vw(9.11),
    alignItems: 'center',
  },
  icn: {color: '#fff', fontSize: vw(6.92)},
  txt: {color: '#fff', fontSize: vw(6)},
});
export default DriveBtn;
