import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/MaterialIcons';

function ScheduleBtn() {
  return (
    <View style={styles.container}>
      <Text style={styles.txt}>Book vehicle</Text>
      <Icon name="trending-flat" style={styles.icn}></Icon>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: vw(75),
    height: vw(15),
    borderWidth: vw(0.6),
    borderColor: '#1d91ca',
    borderRadius: vw(4),
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: vw(9.11),
    alignItems: 'center',
  },
  icn: {color: '#1d91ca', fontSize: vw(6.92)},
  txt: {color: '#1d91ca', fontSize: vw(6)},
});
export default ScheduleBtn;
