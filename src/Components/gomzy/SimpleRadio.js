import React from "react";
import RadioButtonRN from "radio-buttons-react-native";
import { View, StyleSheet, Text, Image } from "react-native";
import Icon  from "react-native-vector-icons/AntDesign";

export default function SimpleRadio(props) {
  const data = [
    {
      label: "Learner",
    },
    {
      label: "Private",
    },
    {
      label: "Commercial",
    },
  ];

  return (
    <>
      <View>
        <RadioButtonRN
          data={data}
          selectedBtn={(e) => console.log(e)}
          initial={1}
          box={false}
          boxStyle={{ width: 100 }}
          textStyle={{ marginLeft:10}}
          circleSize={16}
          textColor={"grey"}
          style={{
            width: 70,
            flexDirection: "row",
            padding: 10,
            marginLeft: 30,
          }}
        />
      </View>
    </>
  );
}
