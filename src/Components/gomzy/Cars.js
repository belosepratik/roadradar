import React, { useState } from "react";
import { View, StyleSheet, Text, Image, TouchableOpacity } from "react-native";
import RadioBtn from './RadioBtn';

import Icon from "react-native-vector-icons/AntDesign";
import Car from '../../assets/constants/images'

export default function Cars(props) {

  const [selectedVehicle, setSelectedVehicle] = useState()

  const { carinfo } = props;
  // console.log(carinfo);

  return (
    <>
      {carinfo.map((d, i) => {
        console.log(d)
        return (
          <>
            <View style={styles.container}>
              <View style={{ flexDirection: "row" }}>
                <View
                  style={{

                    flexDirection: "row",
                    // borderWidth: 1,                 //for refernce in image 
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Image
                    style={{
                      width: 50,
                      height: 50,
                      // borderRadius: 30,
                      marginTop: 25,
                      marginLeft: 35,

                    }}
                    source={d.image}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    marginTop: 25,
                    // borderWidth: 1,
                    marginRight: 10,
                    marginLeft: 20,
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <View style={{ marginLeft: 10 }}>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                      {d.title}
                    </Text>
                    <Text style={{ fontSize: 15, color: "grey" }}>{d.desc}</Text>
                  </View>
                  <TouchableOpacity onPress={() => {
                    console.log(d.id)
                    setSelectedVehicle(d.id)
                  }} >
                    {/* <RadioBtn id={i}/> */}
                    {
                      selectedVehicle == d.id ?
                        <Icon name="checkcircle" size={25} color="green" /> :
                        <Icon name="checkcircleo" size={25} color="green" />

                    }
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            {d.title == "Metro" && <View style={{ marginTop: 20, marginLeft: 30 }}>
              <Text style={{ fontSize: 20 }}>Other Options</Text>
            </View>}
          </>
        );
      })}
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    // borderWidth: 1,
  }
});
