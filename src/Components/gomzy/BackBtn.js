import React from "react";
import { View, Text, StyleSheet } from "react-native";
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from "react-native-responsive-dimensions";
import Icon from "react-native-vector-icons/MaterialIcons";
function BackBtn() {
  return (
    <View style={styles.container}>
      <Icon name="arrow-back" size={vw(8.86)} style={styles.icn}></Icon>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: vw(14.7),
    height: vw(14.33),
    backgroundColor: "#fff",
    borderRadius: vw(4),
    alignItems: "center",
    justifyContent: "center",
  },
});
export default BackBtn;
