import React, { useState } from 'react'
import RadioButtonRN from "radio-buttons-react-native";
import { View, StyleSheet, Text, Image } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";

export default function RadioBtn(props) {
  const { id } = props;
  const [selectedData, SetSelectedData] = useState([])

  console.log(id)

  const data = [
    {
      label: "",
    },

  ];

  return (
    <>
      <View>
        <RadioButtonRN
          data={data}
          selectedBtn={(e) => {
            SetSelectedData({ label: id })
            console.log(selectedData);
          }}
          box={false}
          style={{ marginRight: 40 }}
          icon={<Icon name="checkcircleo" size={25} color="green" />}

        />
      </View>
    </>
  )
}
