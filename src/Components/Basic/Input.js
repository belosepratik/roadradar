import React from 'react'
import { View ,TextInput} from 'react-native'

export default function Input(props, ...rest) {
    let {
        placeholder = 'Enter',
        placeholderTextColor = '#797979',
        style = {},
        keyboardType = 'default',
        HandleData,
        value, defaultValue,
        maxLength,
        onChangeText, onblur,
        secureTextEntry,
        selection
      } = props;
    return (
       
         <TextInput
        // editable={false}
        // selectTextOnFocus={false}
        onChangeText={e => onChangeText(e)}
        // onBlur={onblur}
        onFocus={HandleData}
        value={value}
        maxLength={maxLength}
        selection={selection}
        defaultValue={defaultValue}
        keyboardType={keyboardType}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        style={{ fontSize: 14, ...style, }}
        secureTextEntry={secureTextEntry}
      />
      
    )
}
