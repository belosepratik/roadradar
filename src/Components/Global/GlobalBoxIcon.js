import React from 'react'
import { View } from 'react-native'
import Ant from 'react-native-vector-icons/AntDesign'
export default function GlobalBoxIcon({name}) {
    return (
        <View style={{ padding: 10,borderRadius:10, backgroundColor: 'white', marginRight: 20 }}>
            <Ant name={name} size={25} />
        </View>
    )
}
