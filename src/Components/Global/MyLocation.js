import React from 'react';
import {View} from 'react-native';
import Mt from 'react-native-vector-icons/MaterialIcons';

export default function MyLocation() {
  return (
    <View
      style={{
        padding: 10,
        borderRadius: 10,
        backgroundColor: 'white',
        // marginRight: 10,
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Mt name="my-location" size={25} />
    </View>
  );
}
