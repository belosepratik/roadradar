import React from 'react';
import {TouchableOpacity, Text, View} from 'react-native';
import Ant from 'react-native-vector-icons/Fontisto';
import OnBoarding from '../../Views/OnBoarding/OnBoarding';
import {blue} from '../Constant/Color';
export default function Button({
  txt,
  blueButton,
  oboarding1,
  HandleBoarding,
  navigation,
}) {
  // console.log(HandleBoarding)
  return (
    <TouchableOpacity
      onPress={() => {
        HandleBoarding();
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          padding: 15,
          margin: 10,
          alignItems: 'center',
          backgroundColor: `${blueButton ? blue : 'white'}`,
          borderWidth: 2,
          borderColor: blue,
          borderRadius: 20,
        }}>
        <Text
          style={{
            color: blueButton ? 'white' : blue,
            fontSize: 25,
            fontWeight: '500',
          }}>
          {oboarding1 == 'Explore'
            ? 'Explore'
            : oboarding1 == true
            ? 'Drive'
            : 'Ride'}
        </Text>
        <Ant
          name="arrow-right-l"
          color={blueButton ? 'white' : blue}
          size={30}
        />
      </View>
    </TouchableOpacity>
  );
}
