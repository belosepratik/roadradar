import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/Fontisto';

function DriveBtn({HandleButton}) {
  return (
    <TouchableOpacity style={styles.container} onPress={() => HandleButton()}>
      <Text style={styles.txt}>Drive Now</Text>
      <Icon name="arrow-right-l" style={styles.icn}></Icon>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1d91ca',
    width: vw(80.8),
    height: vw(17.14),
    borderRadius: vw(4),
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: vw(9.11),
    alignItems: 'center',
  },
  icn: {
    color: '#fff',
    fontSize: vw(6.92),
  },
  txt: {color: '#fff', fontSize: vw(6)},
});
export default DriveBtn;
