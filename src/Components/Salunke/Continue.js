import React from "react";
import { View, Text, StyleSheet } from "react-native";
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from "react-native-responsive-dimensions";
import Icon from "react-native-vector-icons/MaterialIcons";

function Continue(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.txt}>Continue</Text>
      <Icon name="trending-flat" style={styles.icn}></Icon>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#1d91ca",
    width: vw(80.8),
    height: vw(17.14),
    borderRadius: vw(4),
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: vw(9.11),
    alignItems: "center",
  },
  icn: { color: "#fff", fontSize: vw(6.92) },
  txt: { color: "#fff", fontSize: vw(6) },
});
export default Continue;
