import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/Fontisto';
import {useDispatch, useSelector} from 'react-redux';
import {setFlow} from '../../Redux/Slice/HomeSlice';

function ScheduleBtn({
  title,
  rowText,
  blueButton,
  navigation,
  goto,
  backScreen,
}) {
  const dispatch = useDispatch();
  const home = useSelector((state) => state.home);

  const {flow} = home;

  return (
    <TouchableOpacity
      onPress={() => {
        console.log('rowText>>>>>>>>>>>', rowText);
        dispatch(setFlow(rowText));
        console.log('rowText>>>>>>>', rowText);
        switch (rowText) {
          case 'Drive':
            navigation.navigate('OnVehicle', {backScreen: backScreen});
            break;
          case 'Schedule Drive':
            navigation.navigate('OnDestinatination', {backScreen: backScreen});
            break;
          case 'Ride':
            navigation.navigate('OnDestinatination', {backScreen: backScreen});
            break;
          case 'Schedule Ride':
            navigation.navigate('OnDestinatination', {backScreen: backScreen});
            break;
        }

        // navigation.navigate(goto, { oboarding1: goto });
      }}
      style={blueButton ? styles.container : styles.containerWhite}>
      <Text style={blueButton ? styles.txt : styles.txtWhite}>{title}</Text>
      <Icon
        name="arrow-right-l"
        style={blueButton ? styles.icn : styles.icnWhite}></Icon>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  //White button
  containerWhite: {
    backgroundColor: '#fff',
    width: vw(80.8),
    height: vw(17.14),
    borderWidth: vw(0.6),
    borderColor: '#1d91ca',
    borderRadius: vw(4),
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: vw(9.11),
    alignItems: 'center',
  },
  icnWhite: {color: '#1d91ca', fontSize: vw(8)},
  txtWhite: {color: '#1d91ca', fontSize: vw(6)},

  // Blue Button
  txt: {color: 'white', fontSize: vw(6)},
  icn: {color: 'white', fontSize: vw(8)},

  container: {
    backgroundColor: '#1d91ca',
    width: vw(80.8),
    height: vw(17.14),
    borderWidth: vw(0.6),
    borderColor: '#1d91ca',
    borderRadius: vw(4),
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: vw(9.11),
    alignItems: 'center',
  },
});
export default ScheduleBtn;
