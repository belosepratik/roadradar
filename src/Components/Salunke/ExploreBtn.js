import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import {setFlowSchedule} from '../../Redux/Slice/HomeSlice';

function ExploreBtn({txt, check, blueButton, navigation, goto, oboarding1}) {
  const dispatch = useDispatch();
  const home = useSelector((state) => state.home);

  const {onboardingStatus, flowSchedule} = home;

  return (
    <TouchableOpacity
      onPress={() => {
        if (onboardingStatus == false && check == 'ScheduleDrive') {
          setFlowSchedule('Schedule Drive');
        } else {
          setFlowSchedule('Schedule Ride');
        }
      }}
      style={blueButton ? styles.container : styles.containerWhite}>
      <Text style={blueButton ? styles.txt : styles.txtWhite}>{txt}</Text>
      <Icon
        name="trending-flat"
        style={blueButton ? styles.icn : styles.icnWhite}></Icon>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  //White button
  containerWhite: {
    backgroundColor: '#fff',
    width: vw(80.8),
    height: vw(17.14),
    borderWidth: vw(0.6),
    borderColor: '#1d91ca',
    borderRadius: vw(4),
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: vw(9.11),
    alignItems: 'center',
  },
  icnWhite: {color: '#1d91ca', fontSize: vw(6.92)},
  txtWhite: {color: '#1d91ca', fontSize: vw(6)},

  // Blue Button
  txt: {color: 'white', fontSize: vw(6)},
  icn: {color: 'white', fontSize: vw(6)},

  container: {
    backgroundColor: '#1d91ca',
    width: vw(80.8),
    height: vw(17.14),
    borderWidth: vw(0.6),
    borderColor: '#1d91ca',
    borderRadius: vw(4),
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: vw(9.11),
    alignItems: 'center',
  },
});
export default ExploreBtn;
