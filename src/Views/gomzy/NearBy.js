import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  View,
  SafeAreaView,
  Text,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Modal from 'react-native-modal';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

import {
  car,
  suv,
  jeep,
  cab,
  metro,
  train,
  bus,
  bike,
  scooter,
  cycle,
} from '../../assets/constants/images';
import BackBtn from '../../Components/gomzy/BackBtn';
import Cars from '../../Components/gomzy/Cars';
import DriveBtn from '../../Components/gomzy/DriveBtn';
import Wrapped from '../../Components/Global/Wrapped';
import Backarrow from 'react-native-vector-icons/Ionicons';
import {BackButton} from '../../Components/Universal/BackButton';

export default function NearBy({navigation, route}) {
  const data = [
    {
      id: '1',
      image: suv,
      title: 'Car - KIA',
      desc: '2 seats available',
    },
    {
      id: '2',
      image: car,
      title: 'Car - Tata Nexon',
      desc: '4 seats available',
    },
    {
      id: '3',
      image: jeep,
      title: 'Jeep - SUV',
      desc: '4 seats available',
    },
  ];

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <>
      <Wrapped>
        <BackButton navigation={navigation} route={route} />
        <MapView
          style={{flex: 1}}
          showsUserLocation={true}
          provider={PROVIDER_GOOGLE}
          initialRegion={{
            latitude: 19.2068505,
            longitude: 72.9528205,
            latitudeDelta: 0.0222,
            longitudeDelta: 0.0121,
          }}></MapView>

        <View
          style={{
            backgroundColor: 'white',
            height: vh(60), // 50% of Screen height
            width: vw(98),
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
            marginLeft: -16,
            // borderWidth:1,
          }}>
          <View style={{marginTop: 20, marginLeft: 40}}>
            <Text style={{fontSize: vf(3)}}>Carpools Nearby</Text>
          </View>
          <View style={{marginTop: 10, marginLeft: 40}}>
            <Text style={{color: 'grey', fontSize: vf(1.8)}}>
              Available on 23,July,2021 - 1:00pm
            </Text>
          </View>

          <View>
            <Cars carinfo={data} />
          </View>

          <View style={{marginTop: 30}}>
            <DriveBtn title="Finish" />
          </View>
        </View>
      </Wrapped>
    </>
  );
}
