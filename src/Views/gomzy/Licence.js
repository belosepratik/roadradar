import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  View,
  SafeAreaView,
  Text,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Modal from 'react-native-modal';

import {
  car,
  suv,
  jeep,
  cab,
  metro,
  train,
  bus,
  bike,
  scooter,
  cycle,
} from '../../assets/constants/images';
import Cars from '../../Components/gomzy/Cars';
import DriveBtn from '../../Components/gomzy/DriveBtn';
import SimpleRadio from '../../Components/gomzy/SimpleRadio';

export default function Licence() {
  const data = [
    {
      id: '1',
      image: scooter,
      title: 'MC 50cc',
      desc: 'Engine Capacity 50cc or less ',
    },
    {
      id: '2',
      image: car,
      title: 'LMV-NT',
      desc: 'Light motor vehicle',
    },
    {
      id: '3',
      image: scooter,
      title: 'FVG',
      desc: 'Vehicles without gears',
    },
    {
      id: '4',
      image: bike,
      title: 'MC EX50cc',
      desc: 'Engine Capacity over 50cc',
    },
    {
      id: '5',
      image: bus,
      title: 'HPMY',
      desc: 'Heavy vehicles with pan permit',
    },
  ];

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <>
      {/* <View style={{ marginTop: 170 }}>
        <BackBtn />
      </View> */}

      <View
        style={{
          backgroundColor: 'white',
          height: vh(100), // 50% of Screen height
          width: vw(98),
          //   borderTopLeftRadius: 25,
          //   borderTopRightRadius: 25,
          // borderWidth:1,
          //   marginTop: 0,
          marginLeft: -16,
        }}>
        <View
          style={{
            marginTop: 30,
            marginLeft: 35,
            marginRight: 50,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View>
            <Text style={{fontSize: 35, fontWeight: 'bold'}}>
              Driving licence
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{fontSize: 15, color: 'grey'}}>Skip</Text>
            <Ionicons name="chevron-forward" size={15} color="grey" />
          </View>
        </View>

        <View style={{marginTop: 20, marginLeft: 35}}>
          <Text style={{fontSize: 25}}>Licence's type</Text>
        </View>

        <View>
          <SimpleRadio />
          {/* <Testw/> */}
        </View>

        <View style={{marginTop: 20, marginLeft: 35}}>
          <Text style={{fontSize: 25}}>Licence's category</Text>
        </View>
        <View>
          <Cars carinfo={data} />
        </View>

        <View style={{marginTop: 30}}>
          <DriveBtn title="Continue" />
        </View>
      </View>
    </>
  );
}
