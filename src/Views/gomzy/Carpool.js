import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  View,
  SafeAreaView,
  Text,
  TouchableHighlight,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Modal from 'react-native-modal';
import {
  car,
  suv,
  jeep,
  cab,
  metro,
  train,
  bus,
  bike,
  scooter,
  cycle,
} from '../../assets/constants/images';
//import BackBtn from '../../Components/gomzy/BackBtn';
import {BackButton} from '../../Components/Universal/BackButton';
import Cars from '../../Components/gomzy/Cars';
import DriveBtn from '../../Components/gomzy/DriveBtn';
import Wrapped from '../../Components/Global/Wrapped';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Navigation from '../../Routes/Navigation';
import {useSelector} from 'react-redux';

export default function Carpool({navigation, route}) {
  console.log('Carpool Current >>>>>>>>>>>', route.name);
  console.log('Carpool Prev >>>>>>>>>>>', route.params.backScreen);

  const home = useSelector((state) => state.home);
  const {flow} = home;

  const data = [
    {
      id: '1',
      image: bus,
      title: 'Bus',
      desc: '₹50 - approximately',
    },
    {
      id: '2',
      image: train,
      title: 'Train',
      desc: '₹214 - approximately',
    },
    {
      id: '3',
      image: metro,
      title: 'Metro',
      desc: '₹300 - approximately',
    },
    {
      id: '4',
      image: cab,
      title: 'Cab/Taxi',
      desc: '₹400 - approximately',
    },
    {
      id: '5',
      image: cycle,
      title: 'Bicycle',
      desc: '₹150 - approximately',
    },
  ];

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const HandleButton = () => {
    // navigation.navigate("")
  };
  return (
    <>
      <Wrapped>
        <BackButton navigation={navigation} route={route} />
        <MapView
          style={{flex: 1}}
          showsUserLocation={true}
          provider={PROVIDER_GOOGLE}
          initialRegion={{
            latitude: 19.2068505,
            longitude: 72.9528205,
            latitudeDelta: 0.0222,
            longitudeDelta: 0.0121,
          }}></MapView>

        <View style={{flex: 5}}>
          <ScrollView>
            <View
              style={{
                height: vh(100), // 50% of Screen height
                width: vw(100),
                //  borderWidth: 1,
                flex: 1,
                flexDirection: 'column',
                marginLeft: 0,
                borderTopLeftRadius: 25,
                borderTopRightRadius: 25,
                elevation: 3,
              }}>
              <View style={{marginTop: 10, marginLeft: 25}}>
                <Text
                  style={{
                    fontSize: 30,
                    fontFamily: 'raleway',
                    fontWeight: '600',
                  }}>
                  Choose your Ride
                </Text>
              </View>
              <View style={{marginTop: 20, marginLeft: 30}}>
                <Text style={{fontSize: 20}}>Public Transport</Text>
              </View>

            <View>
              <Cars carinfo={data} />
            </View>
            {/* 
            <View style={{ marginTop: 20, marginLeft: 30 }}>
              <Text style={{ fontSize: 20 }}>Other Options</Text>
            </View> */}
            {/* <View>
              <Cars carinfo={data2} />
            </View> */}

              <View style={{marginTop: 20}}>
                <DriveBtn title="Book" HandleButton={HandleButton} />
              </View>
            </View>
          </ScrollView>
        </View>
      </Wrapped>
    </>
  );
}
