import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/Feather';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  View,
  SafeAreaView,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import Modal from 'react-native-modal';
import {
  car,
  suv,
  jeep,
  cab,
  metro,
  train,
  bus,
  bike,
  scooter,
  cycle,
  car10,
  cab10,
} from '../../assets/constants/images';
import Cars from '../../Components/gomzy/Cars';
import DriveBtn from '../../Components/gomzy/DriveBtn';
import ScheduleBtn from '../../Components/gomzy/ScheduleBtn';
import Wrapped from '../../Components/Global/Wrapped';
import {useSelector} from 'react-redux';

export default function Vehicle({navigation, route}) {
  console.log('Vehicle Current >>>>>>>>>>>', route.name);
  console.log('Vehicle Prev >>>>>>>>>>>', route.params.backScreen);

  console.log('Vehicle Current >>>>>>>>>>>', route.name);
  console.log('Vehicle Prev >>>>>>>>>>>', route.params.backScreen);

  const home = useSelector((state) => state.home);
  const {flow} = home;

  const data = [
    {
      id: '1',
      image: car10,
      title: 'Car - Tata Nexon',
      desc: 'Four Wheeler',
    },
    {
      id: '2',
      image: cab10,
      title: 'Taxi - Maruti Swift',
      desc: 'Four Wheeler',
    },
    {
      id: '3',
      image: bike,
      title: 'Bike - Yamaha',
      desc: 'Two Wheeler',
    },
    {
      id: '4',
      image: scooter,
      title: 'Scooty - Activa',
      desc: 'Two Wheeler',
    },
  ];

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const HandleButton = () => {
    navigation.navigate('OnDestinatination', {backScreen: route.name});
  };

  const HandleBack = () => {
    navigation.navigate(screen);
  };

  return (
    <Wrapped>
      <MapView
        style={{flex: 1}}
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 19.2068505,
          longitude: 72.9528205,
          latitudeDelta: 0.0222,
          longitudeDelta: 0.0121,
        }}></MapView>

      <View
        style={{
          flex: 6,
          flexDirection: 'column',
          elevation: 3,
          borderTopLeftRadius: 18,
          borderTopRightRadius: 18,
        }}>
        {/* <View
          style={{
            padding: 10,
            flex: 1,
            justifyContent: 'flex-end',
            // height: vh(100), // 50% of Screen height
            // width: vw(98),
            borderTopLeftRadius: 50,
            borderTopRightRadius: 50,
            marginLeft: -16,
            // borderWidth:1,
            // marginTop: 250,
          }}>
          <View style={{position: 'absolute', top: '1%', left: '8%'}}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <Ionicons name="chevron-back" size={40} color="black" />
            </TouchableOpacity>
          </View>
          <View style={{marginTop: 10, marginLeft: 30}}>
            <Text style={{fontSize: responsiveFontSize(3.5)}}>
              Pick your vehicle
            </Text>
            <Text style={{color: 'grey'}}>Vehicles Owned - 4</Text>
          </View>

          {/* <View> */}
            {/* <Image
              // style={styles.tinyLogo}
              source={require('../../assets/image/cab10.png')}
              style={{
                // height: responsiveHeight(5),
                // width: responsiveWidth(11),
                height: 20,
                width: 20,
              }}
            /> */}
            <Cars carinfo={data} />
          {/* </View> */}

        <View>
          <Cars carinfo={data} />
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            marginTop: 5,
            justifyContent: 'space-evenly',
          }}>
          <DriveBtn title="Choose" HandleButton={HandleButton} />
          <ScheduleBtn />
        </View>
        {/* <View style={{}}>
          <DriveBtn title="Choose" HandleButton={HandleButton} />
        </View>
        <View style={{marginTop: 20}}>
          <ScheduleBtn />
        </View> */}
      </View>
      {/* </View> */}
    </Wrapped>
  );
}
