import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Touchable,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import {useSelector} from 'react-redux';
import Wrapped from '../../Components/Global/Wrapped';
import DriveBtn from '../../Components/Salunke/DriveBtn';
import ScheduleBtn from '../../Components/Salunke/ScheduleBtn';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Backarrow from 'react-native-vector-icons/Ionicons';

function Gtr({navigation, route}) {
  const home = useSelector((state) => state.home);
  const {flow} = home;

  const {oboarding1} = route.params;
  console.log('oboarding1>>>>>>>', oboarding1);

  return (
    <Wrapped>
      <View
        style={{
          backgroundColor: 'white',
          height: 100,
          width: '96%',
          right: '2%',
          left: '2%',
          position: 'absolute',
          top: '2%',
          zIndex: 2,
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          borderRadius: 12,
          //elevation: 5,
        }}>
        <View>
          <Text>Carpool</Text>
          <Text>150m</Text>
        </View>
        <View>
          <Text>Pothole</Text>
          <Text>200m</Text>
        </View>
        <View>
          <Text>Traffic Signal</Text>
          <Text>500m</Text>
        </View>
      </View>
      <MapView
        style={{flex: 1}}
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 19.2068505,
          longitude: 72.9528205,
          latitudeDelta: 0.0222,
          longitudeDelta: 0.0121,
        }}></MapView>
      <View
        style={{
          backgroundColor: 'red',
          borderTopStartRadius: 20,
          borderTopRightRadius: 20,
          height: 100,
          width: '96%',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View>
          <Text>PICKED UP</Text>
          <Text>0/3</Text>
        </View>
        <View>
          <Text>TIME LEFT</Text>
          <Text>14</Text>
        </View>
        <View>
          <Text>DISTANCE</Text>
          <Text>2.3km</Text>
        </View>
      </View>
    </Wrapped>
  );
}
const styles = StyleSheet.create({
  btnCont: {marginTop: vh(5)},
  container: {
    height: vh(41),
    borderTopLeftRadius: vw(8),
    borderTopRightRadius: vw(8),
    backgroundColor: '#fff',
    paddingHorizontal: vw(10),
    // backgroundColor: 'red',
    marginBottom: 20,
  },
  heading: {
    fontSize: vh(4),
  },
  mg: {marginTop: vh(1.85)},
  sub: {
    color: '#5e5e5e',
    fontSize: vh(2.1),
    marginTop: vh(0.5),
  },
});
export default Gtr;
