import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import {useSelector} from 'react-redux';
import Wrapped from '../../Components/Global/Wrapped';
import DriveBtn from '../../Components/Salunke/DriveBtn';
import ScheduleBtn from '../../Components/Salunke/ScheduleBtn';
import Backarrow from 'react-native-vector-icons/Ionicons';
import {BackButton} from '../../Components/Universal/BackButton';

function Gtr({navigation, route}) {
  console.log('Gtr Current >>>>>>>>>>>', route.name);
  console.log('Gtr Prev >>>>>>>>>>>', route.params.backScreen);

  const home = useSelector((state) => state.home);
  const {flow} = home;

  // console.log('oboarding1>>>>>>>', oboarding1);

  const driveData = [
    {id: 1, title: 'Drive Now', rowText: 'Drive', btnStyle: true},
    {id: 2, title: 'Schedule', rowText: 'Schedule Drive', btnStyle: false},
  ];

  const rideData = [
    {id: 1, title: 'Ride Now', rowText: 'Ride', btnStyle: true},
    {id: 2, title: 'Schedule', rowText: 'Schedule Ride', btnStyle: false},
  ];

  return (
    <Wrapped>
      <BackButton navigation={navigation} route={route} />
      <MapView
        style={{flex: 1}}
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 19.2068505,
          longitude: 72.9528205,
          latitudeDelta: 0.0222,
          longitudeDelta: 0.0121,
        }}></MapView>
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-end',
          elevation: 2,
          // borderWidth: 1,
          borderTopLeftRadius: vw(6),
          borderTopRightRadius: vw(6),
        }}>
        {/* <View style={styles.container}> */}
        <View style={styles.textView}>
          <Text style={styles.heading}>Getting things ready</Text>
          <Text style={styles.sub}>When do you want to drive?</Text>
        </View>

        <FlatList
          data={
            flow == 'Drive' || flow == 'Schedule Drive' ? driveData : rideData
          }
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{paddingTop: 20}}
          renderItem={({item}) => (
            <>
              <View style={{marginTop: vh(3)}}>
                <ScheduleBtn
                  title={item.title}
                  rowText={item.rowText}
                  blueButton={item.btnStyle}
                  // goto={oboarding1 ? 'OnVehicle' : 'OnDestinatination'}
                  navigation={navigation}
                  backScreen={route.name}
                />
              </View>
            </>
          )}
        />

        {/* <View style={{ marginTop: vh(3) }}>
            <ScheduleBtn
              text={oboarding1 ? 'Drive Now' : 'Ride Now'}
              txt={oboarding1 ? 'Drive Now' : 'Ride Now'}
              blueButton={true}
              goto={oboarding1 ? 'OnVehicle' : 'OnDestinatination'}
              navigation={navigation}
            />
          </View>

          <View style={{ marginTop: vh(3) }}>
            <ScheduleBtn
              text={oboarding1 ? 'Schedule Now' : 'Schedule Now'}
              txt="Schedule"
              blueButton={false}
              goto={oboarding1 ? 'OBDest' : 'OnDestinatination'}
              navigation={navigation}
            />
          </View> */}
        {/* </View> */}
      </View>
    </Wrapped>
  );
}
const styles = StyleSheet.create({
  textView: {
    flex: 1,
    // backgroundColor: 'red',
    marginTop: vh(3.5),
    marginLeft: vh(3.5),
  },
  heading: {
    fontSize: vf(3),
    fontWeight: '600',
    fontFamily: 'raleway',
  },
  sub: {
    color: '#565656',
    fontSize: vf(1.8),
    marginTop: vh(0.5),
  },
});
export default Gtr;
