import React from "react";
import { View, Text, StyleSheet } from "react-native";
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from "react-native-responsive-dimensions";
import Icon from "react-native-vector-icons/MaterialIcons"; 
import Continue from "../../Components/Salunke/Continue";


// import { TimePicker } from "react-native-simple-time-picker";

function Sfl(props) {
  //   const [hours, setHours] = React.useState(0);
  //   const [minutes, setMinutes] = React.useState(0);
  //   const handleChange = (value: {hours: number, minutes: number}) => {
  //       setHours(value.hours);
  //       setMinutes(value.minutes);
  //     };
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.heading}>Schedule for later</Text>
        <Text style={styles.sub}>Choose time</Text>
      </View>
      <View style={styles.clock}>{/* <TimePicker /> */}</View>
      <View style={{ flexDirection: "row", marginTop: vw(5) }}>
        <Icon name="radio-button-unchecked" size={vw(8)} color="#1d91ca" />
        <Text style={{ fontSize: vw(5), marginLeft: vw(2) }}>
          Tommorow - 01:00 pm
        </Text>
      </View>
      <View style={{ flexDirection: "row", marginTop: vw(4) }}>
        <View>
          <Icon name="radio-button-checked" size={vw(8)} color="#ff715b" />
          <Icon name="more-vert" size={vw(8)} color="#000" />
          <Icon name="location-on" size={vw(8)} color="#1d91ca" />
        </View>
        <View
          style={{
            flexDirection: "column",
            justifyContent: "space-evenly",
            marginLeft: 20,
          }}
        >
          <Text style={{ fontSize: vw(4.2) }}>A/15 Rastriy Shala Chowk</Text>
          <Text style={{ fontSize: vw(4.2) }}>Shivaji Marg - 3rd turn</Text>
        </View>
      </View>
      <View style={{ marginTop: vh(4.25) }}>
        <Continue />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    height: vh(87),
    backgroundColor: "#fff",
    borderTopLeftRadius: vw(10),
    borderTopRightRadius: vw(10),
    paddingHorizontal: vw(10),
  },
  clock: { height: vh(42), borderWidth: 2 },
  heading: {
    fontSize: vh(4),
  },
  sub: {
    color: "#5e5e5e",
    fontSize: vh(2.1),
    marginTop: vh(0.5),
  },
});
export default Sfl;
