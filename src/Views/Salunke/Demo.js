import React from 'react';
import {Text, View} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import BackBtn from '../../Components/Salunke/BackBtn';
import Gtr from './Gtr';

function Demo() {
  return (
    <View
      style={{
        backgroundColor: 'gold',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',

        borderTopLeftRadius: vw(10),
      }}>
      <View
        style={{
          position: 'absolute',
          top: vh(3.14),
          left: vh(3.14),
        }}>
        <BackBtn />
      </View>
      <View style={{backgroundColor: 'yellow'}}>
        <Gtr />
      </View>
    </View>
  );
}

export default Demo;
