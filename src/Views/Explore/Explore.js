import React, {useEffect, useState} from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
// import { Provider } from 'react-redux';
// import { store, persistor } from './src/Redux/store';
// import { PersistGate } from 'redux-persist/integration/react';
// import { persistStore } from 'redux-persist';
// import Navigation from './src/Routes/Navigation';
import {View, Text, Image, Touchable} from 'react-native';
import Fn from 'react-native-vector-icons/FontAwesome5';
import HomeIcon from '../../../src/assets/IconComponent/HomeIcon';
import Ant from 'react-native-vector-icons/AntDesign';
import {Button, Footer, FooterTab, Icon, Thumbnail, Badge} from 'native-base';
import {grey} from 'ansi-colors';
import {TouchableOpacity} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';

const Explore = ({navigation}) => {
  // console.log('Explore Current >>>>>>>>>>>', route.name);
  // console.log('Explore Prev >>>>>>>>>>>', route.params.backScreen);

  const name = 'Adesh';
  return (
    <View>
      <View style={{padding: 25}}>
        <TouchableOpacity onPress={() => navigation.navigate('OnBoarding')}>
          <View>
            <Fn name="chevron-left" size={25} />
          </View>
        </TouchableOpacity>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 30, fontWeight: '600'}}>Hey {name}</Text>
          <Text style={{color: '#565656', fontSize: 16}}>
            Welcome to the explore section
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 20,
          }}>
          <TouchableOpacity
            style={{
              backgroundColor: '#FFA9AD',
              borderRadius: 20,
              width: '45%',
              height: '55%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={require('../../../src/assets/food.png')}
              style={{
                width: responsiveWidth(14),
                height: responsiveHeight(9),
              }}></Image>
            <Text style={{textAlign: 'center'}}>Food</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: '#ffd992',
              borderRadius: 20,
              width: '45%',
              height: '55%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={require('../../../src/assets/place.png')}
              style={{
                width: responsiveWidth(14),
                height: responsiveHeight(9),
              }}></Image>
            <Text style={{textAlign: 'center'}}>Place</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: -80,
          }}>
          <TouchableOpacity
            style={{
              backgroundColor: '#8dc9ff',
              borderRadius: 20,
              width: '45%',
              height: '55%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={require('../../../src/assets/chat.png')}
              style={{
                width: responsiveWidth(14),
                height: responsiveHeight(9),
              }}></Image>
            <Text style={{textAlign: 'center'}}>Chat</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              backgroundColor: '#d8b9ff',
              borderRadius: 20,
              width: '45%',
              height: '55%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={require('../../../src/assets/help-line.png')}
              style={{
                width: responsiveWidth(14),
                height: responsiveHeight(9),
              }}></Image>
            <Text style={{textAlign: 'center'}}>Help</Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: -80,
          }}>
          <TouchableOpacity
            style={{
              backgroundColor: '#91fee9',
              borderRadius: 20,
              width: '45%',
              height: '55%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={require('../../../src/assets/report.png')}
              style={{
                width: responsiveWidth(14),
                height: responsiveHeight(9),
              }}></Image>
            <Text style={{textAlign: 'center'}}>Report</Text>
          </TouchableOpacity>
        </View>
      </View>
      <BottomTabs />
    </View>
  );
};

export default Explore;
