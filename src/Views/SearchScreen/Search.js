import React, { createRef, useRef, useState } from 'react';
import {
  Image,
  View,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TextInput
} from 'react-native';
import Header from '../../Components/Header';
import styles from '../../Components/Styles/Styles';
import DestinationSearch from '../../Components/AppComponents/SearchComponents/DestinationSearch';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ev from 'react-native-vector-icons/EvilIcons';
import Timline from '../../Components/AppComponents/SearchComponents/SearchPageComp/Timline';
import ActionSheet from 'react-native-actions-sheet';
import { windowHeight } from '../../Util/Dimensions';
import SearchActionSheet from '../../Components/Actionsheet/SearchActionSheet';

const data = [
  {
    time: '09:00',
    title: 'Your location',
    lineColor: '#009688',
    button: false,
    icon: (
      <Ev name="location" />
    )
  },
  {
    time: '10:45',
    title: 'Select next stop',
    button: true,
    icon: (
      <MaterialCommunityIcons name="bus-stop" />
    )
  },

]


const SelectRoute = [
  {
    name: 'Cab To gateway',
    id: 1,
  },
  {
    name: 'Rickshaw',
    id: 2,
  },
  {
    name: 'Bus',
    id: 3,
  },
  {
    name: 'Show Route',
    id: 4,
  },
  {
    name: 'Show Refreshment',
    id: 5,
  },
  {
    name: 'Show Best Option',
    id: 6,
  },


]
function Search({ navigation }) {
  const actionSheetRef = useRef();
  const [Timeroute, setTimeroute] = useState([])
  const HandleRoute = (item) => {
    setTimeroute(item)
    actionSheetRef.current?.setModalVisible();
    console.log(actionSheetRef.current)
  }

  // console.log("Timeroute",Timeroute)
  return (
    <SafeAreaView >
      {/* <Header title="Home" navigation={navigation} noFab={true} route={route}> */}
      <ScrollView>
        <View style={{ justifyContent: 'flex-start', marginLeft: 10, marginRight: 10 }}>
          <DestinationSearch />
        </View>

        <Timline SelectRoute={SelectRoute} data={data} navigation={navigation}
          HandleRoute={HandleRoute} />
        {/* </Header> */}
      </ScrollView>
      <ActionSheet
        ref={actionSheetRef}
        headerAlwaysVisible={true}
        statusBarTranslucent
        containerStyle={{ height: windowHeight / 2 }}
        bounceOnOpen={true}>
        <SearchActionSheet
          Timeroute={Timeroute}
          navigation={navigation}
        />
      </ActionSheet>
    </SafeAreaView>
  );
}
export default Search;
