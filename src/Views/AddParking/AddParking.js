import React, { useEffect, useState, createRef } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';
import { store, persistor } from '../../Redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';
import Navigation from '../../Routes/Navigation';
import { View, Text, Image, ScrollView, Switch } from 'react-native';
import Fn from 'react-native-vector-icons/FontAwesome5';
import HomeIcon from '../../assets/IconComponent/HomeIcon';
import Ant from 'react-native-vector-icons/AntDesign';
import { MAP_KEY } from '../.././../Config';
import { Button, Footer, FooterTab, Icon, Thumbnail, Badge } from 'native-base';
import OnlineIcon from '../../assets/IconComponent/OnlineIcon';
import OfflineIcon from '../../assets/IconComponent/OfflineIcon';
import { grey } from 'ansi-colors';
import { TouchableOpacity } from 'react-native';
import ActionSheet from 'react-native-actions-sheet';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import MapViewDirections from 'react-native-maps-directions';
import Wrapped from '../../Components/Global/Wrapped';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { windowWidth } from '../../Util/Dimensions';

const actionSheetRef = createRef();

const AddParking = () => {
  const [showButton, setShowButton] = React.useState(false);
  let actionSheet;

  const [origin, setorigin] = useState();
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  const toggleSwitch2 = () => setIsEnabled2((previousState) => !previousState);
  const [isEnabled2, setIsEnabled2] = useState(false);
  return (
    <View
      style={{
        justifyContent: 'flex-start',
        flex: 1,
      }}>
      {showButton && (
        <View style={{ justifyContent: 'flex-start' }}>
          {/* <TouchableOpacity style={{backgroundColor:'#FFFFFF',width:'18%',height:'30%',borderRadius:10,
  alignItems: 'center',justifyContent:'center'}}>
    <Fn 
    name="arrow-left" size={30} />
  </TouchableOpacity> */}
        </View>
      )}
      <TouchableOpacity
        onPress={() => {
          actionSheetRef.current?.setModalVisible();
          setShowButton(true);
        }}>
        <Text style={{ textAlign: 'center', marginTop: 25 }}>
          Open ActionSheet
        </Text>
      </TouchableOpacity>

      <ActionSheet
        ref={actionSheetRef}
        containerStyle={{ backgroundColor: 'white', borderRadius: 35 }}>
        <View style={{ padding: 20 }}>
          <View
            style={{
              backgroundColor: 'white',
              borderRadius: 35,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text style={{ fontSize: 35, fontWeight: '900' }}>Add Parking</Text>
            <TouchableOpacity>
              <Text style={{ fontSize: 25, color: 'grey' }}>
                Skip <Fn name="chevron-right" size={30} />
              </Text>
            </TouchableOpacity>
          </View>
          <View>
            <View>
              <View style={{ padding: 15 }}>
                <Text style={{ fontSize: 20 }}>I own a parking spot</Text>

                <Switch
                  style={{ marginTop: -25 }}
                  trackColor={{ false: '#767577', true: '#81b0ff' }}
                  thumbColor={isEnabled ? '#FFFF' : '#FFFF'}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={toggleSwitch}
                  value={isEnabled}
                />
              </View>
              <View style={{ padding: 15 }}>
                <Text style={{ fontSize: 18, fontWeight: '900' }}>
                  Choose Type
                </Text>
              </View>
              <View
                style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                <TouchableOpacity
                  style={{
                    alignItems: 'center',
                    borderWidth: 1,
                    borderRadius: 10,
                    width: '30%',
                    height: '100%',
                  }}>
                  <Image source={require('../../assets/Add-Parking/car.png')} />
                  <Text>Four Wheeler</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    alignItems: 'center',
                    borderWidth: 1,
                    borderRadius: 10,
                    width: '30%',
                    height: '100%',
                  }}>
                  <Image
                    source={require('../../assets/Add-Parking/bike.png')}
                  />
                  <Text>Tow Wheeler</Text>
                </TouchableOpacity>
              </View>
              <View style={{ padding: 15 }}>
                <Text style={{ fontSize: 25 }}>Upload Image</Text>
                <View
                  style={{
                    borderWidth: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderStyle: 'dashed',
                    borderRadius: 20,
                    height: '35%',
                    marginTop: 35,
                  }}>
                  <TouchableOpacity>
                    <Image
                      source={require('../../assets/Add-Parking/upload.png')}
                    />
                  </TouchableOpacity>
                  <Text>Tap to choose</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    padding: 15,
                  }}>
                  <Image
                    style={{ flex: 0.5, borderRadius: 10, height: '170%' }}
                    source={require('../../assets/Add-Parking/uploadimg.png')}
                  />
                  <Text style={{ fontSize: 20 }}>Parking-Image.jpp</Text>
                  <TouchableOpacity>
                    <Fn style={{ color: 'red' }} name="trash" size={18} />
                  </TouchableOpacity>
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text style={{ fontSize: 25 }}>Share Location</Text>
                  <Switch
                    style={{ marginTop: -25 }}
                    trackColor={{ false: '#767577', true: '#81b0ff' }}
                    thumbColor={isEnabled ? '#FFFF' : '#FFFF'}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={toggleSwitch}
                    value={isEnabled}
                  />
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text style={{ fontSize: 25 }}>EV charging available </Text>
                  <Switch
                    style={{ marginTop: -25 }}
                    trackColor={{ false: '#767577', true: '#81b0ff' }}
                    thumbColor={isEnabled2 ? '#FFFF' : '#FFFF'}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={toggleSwitch2}
                    value={isEnabled2}
                  />
                </View>
                <View style={{ marginTop: 20 }}>
                  <TouchableOpacity
                    style={{
                      flexDirection: 'row',
                      backgroundColor: '#1E8FE7',
                      padding: 10,
                      borderRadius: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{ color: 'white', fontSize: 28, marginLeft: 25 }}>
                      Continue
                    </Text>
                    <Fn
                      name="arrow-right"
                      style={{ color: 'white', marginRight: 30 }}
                      size={25}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </ActionSheet>
    </View>
  );
};

export default AddParking;
