import React, { createRef } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Image,
  TextInput,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  Button,
  Modal,
  View,
  CheckBox,
  ImageBackground,
} from 'react-native';
import { RadioButton } from 'react-native-paper';
import Fn from 'react-native-vector-icons/FontAwesome5';
import Arrow from 'react-native-vector-icons/FontAwesome';
import DriveBtn from './DriveBtn';

const actionSheetRef = createRef();

const AddVehicle = () => {
  const [checked, setChecked] = React.useState('first');
  return (
    <SafeAreaView>
      <ScrollView>
        <View
          style={{
            margin: 20,

          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              margin: 10,
              alignItems: 'center',
            }}>
            <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Add Vehicle</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ color: '#707070', marginRight: 5 }}>Skip</Text>
              <Fn name="chevron-right" color="#707070" />
            </View>
          </View>
          <View style={{ margin: 10 }}>
            <Text style={{ fontSize: 25 }}>Vehicle Type</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
              <RadioButton
                value="first"
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('first')}
                color="#1D91CA"></RadioButton>
              <Text>Private</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <RadioButton
                value="first"
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('first')}
                color="#1D91CA"></RadioButton>
              <Text>Commercial</Text>
            </View>
          </View>
          <View
            style={{
              padding: 5,
              margin: 10,
              marginLeft: -5,
              flexDirection: 'row',
              justifyContent: 'space-around',
            }}>
            <View
              style={{
                borderWidth: 2,
                borderColor: '#C2C1C1',
                borderRadius: 10,
                padding: 20,
                alignItems: 'center',
              }}>
              <Image
                style={{
                  height: 78.15,
                  width: 86.71,
                  padding: 15,
                  alignItems: 'center',
                }}
                source={require('../../assets/Rectangle30.png')}

              />
              <Text style={{ textAlign: 'center', paddingTop: 10, fontWeight: 'bold' }}>
                Two Wheeler
              </Text>
            </View>
            <View
              style={{
                borderWidth: 2,
                borderColor: '#C1C1C1',
                borderRadius: 10,
                padding: 20,
                alignItems: 'center',
              }}>
              <Image
                style={{
                  height: 78.15,
                  width: 86.71,
                  padding: 15,
                  alignItems: 'center',
                }}
                source={require('../../assets/Rectangle33.png')}

              />
              <Text style={{ textAlign: 'center', paddingTop: 10, fontWeight: 'bold' }}>Three Wheeler</Text>
            </View>
          </View>

          <View
            style={{
              padding: 5,
              margin: 10,
              marginLeft: -5,
              flexDirection: 'row',
              justifyContent: 'space-around',
            }}>
            <View
              style={{
                borderWidth: 2,
                borderColor: '#C2C1C1',
                borderRadius: 10,
                padding: 20,
                alignItems: 'center',
              }}>
              <Image
                style={{
                  height: 78.15,
                  width: 86.71,
                  padding: 15,
                  alignItems: 'center',
                }}
                source={require('../../assets/Rectangle35.png')}

              />
              <Text style={{ textAlign: 'center', paddingTop: 10, fontWeight: 'bold' }}>Four Wheeler</Text>
            </View>
            <View
              style={{
                borderWidth: 2,
                borderColor: '#C1C1C1',
                borderRadius: 10,
                padding: 20,
                alignItems: 'center',
              }}>
              <Image
                style={{
                  height: 78.15,
                  width: 86.71,
                  padding: 15,
                  alignItems: 'center',
                }}
                source={require('../../assets/Rectangle37.png')}

              />
              <Text style={{ textAlign: 'center', paddingTop: 10, fontWeight: 'bold' }}>Other</Text>
            </View>
          </View>
          <View
            style={{
              padding: 5,
              margin: 10,
              marginLeft: -5,
              flexDirection: 'row',
            }}>
            <View
              style={{
                borderWidth: 2,
                borderColor: '#C2C1C1',
                borderRadius: 10,
                padding: 20,
                margin: 20,
                marginTop: 0,
              }}>
              <Image
                style={{
                  height: 78.15,
                  width: 86.71,
                  padding: 15,
                  alignItems: 'center',
                }}
                source={require('../../assets/Rectangle30.png')}

              />
              <Text style={{ textAlign: 'center', paddingTop: 10, fontWeight: 'bold' }}>SUV</Text>
            </View>
          </View>
          <View >
            <TouchableOpacity>
              <DriveBtn title="Continue" />
            </TouchableOpacity>
          </View>

        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AddVehicle;
