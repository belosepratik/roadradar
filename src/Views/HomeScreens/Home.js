import React, { useState } from 'react'
import {
  Image,
  View,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  Text,
  TextInput,
  Pressable,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { Avatar } from 'react-native-paper';
import Header from '../../Components/Header';
import HomeIcon from '../../assets/IconComponent/HomeIcon';
import OnlineIcon from '../../assets/IconComponent/OnlineIcon';
import OfflineIcon from '../../assets/IconComponent/OfflineIcon';
import styles from '../../Components/Styles/Styles';

import SwitchSelector from 'react-native-switch-selector';
// import Icon from 'react-native-vector-icons/AntDesign'
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import MapViewDirections from 'react-native-maps-directions';
import Geocoder from 'react-native-geocoding';
import { useDispatch, useSelector } from 'react-redux';
import { GeoDecoding } from '../../functions/GeoDecording';
import { setmyLocation } from '../../Redux/Slice/HomeSlice';
import { MAP_KEY } from '../../../Config';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import WelcomeComponent from '../../Components/AppComponents/HomeComponents/WelcomeComponent';
import Wrapped from '../../Components/Global/Wrapped';
import { windowWidth } from '../../Util/Dimensions';
import HomeAbsoluteCopmponents from '../../Components/AppComponents/HomeComponents/HomeAbsoluteCopmponents';
import { blue } from '../../Components/Constant/Color';
import { setdestlatlong } from '../../Redux/Slice/LoginSlice';
import Icon from '../../Components/Constant/Icon'


function Home({ navigation, route }) {
  const dispatch = useDispatch()
  const login = useSelector(state => state.login)
  const { location } = login
  const home = useSelector(state => state.home)
  const { myLocation, destlatlong } = home

  const [origin, setorigin] = useState()
  const [dest, setdest] = useState()

  let key = {
    start: 0,
    end: 0
  }

  const traveloptions = [
    { label: 'Travel', value: 'Travel' },
    { label: 'Park', value: 'Park' },
  ];

  const traveloptions1 = [
    { label: '', value: 'Travel', imageIcon: wifi },
    { label: 'Park', value: 'Park' },
  ];
  const wifi1 = (
    <HomeIcon fill={"black"} width={30} height={30} />
  )

  const wifi = (
    <OnlineIcon fill={"black"} width={30} height={30} />
  );
  const off = (
    <OfflineIcon fill={"black"} width={30} height={30} />
  )

  const goToSearch = () => {
    navigation.navigate('DestinationSearch');
  };


  // React.useEffect(() => {
  //   GeoDecoding(location.latitude, location.longitude, MAP_KEY)
  //     .then((res) => {
  //       dispatch(setmyLocation(res))
  //       // console.log(res)
  //     })
  // }, [])

  React.useEffect(() => {
    if (location) {
      const originLocation = {
        latitude: location.latitude, longitude: location.longitude
      }
      setorigin(originLocation)
    }
    if (destlatlong) {
      const destLocation = {
        latitude: destlatlong.lat, longitude: destlatlong.lng
      }
      setdest(destLocation)
    }


  }, [destlatlong, location])


  return (
    <Wrapped>
      <MapView
        style={{ flex: 1 }}
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 19.2068505,
          longitude: 72.9528205,
          latitudeDelta: 0.0222,
          longitudeDelta: 0.0121,
        }}>
        {/* <Marker
          coordinate={origin}
          image={Icon.icCurLoc}
        /> */}
        {
          dest &&
          <Marker
            coordinate={dest}
            image={Icon.icGreenMarker}
          />
        }
        {
          dest &&
          <MapViewDirections
            origin={origin}
            destination={dest}
            apikey={MAP_KEY}
            strokeWidth={3}
            strokeColor='green'
          />
        }

      </MapView>
      <View style={{ position: 'absolute', width: windowWidth }}>
        <HomeAbsoluteCopmponents wifi={wifi} wifi1={wifi1} off={off} traveloptions={traveloptions} />
      </View>
    </Wrapped>
  );
}
export default Home;


const styles1 = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    // backgroundColor: 'black',
  },
  searchBox: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 40 : 10,
    flexDirection: 'row',
    // backgroundColor: '#fff',
    // width: '90%',
    // alignSelf:'center',
    borderRadius: 5,
    paddingVertical: 20,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    // elevation: 10,
  },
  ham: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 40 : 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    // width: '90%',
    // alignSelf:'center',
    borderRadius: 5,
    padding: 10,
    margin: 10,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  searchBtn: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 200 : 120,
    flexDirection: 'row',
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 15,
    padding: 10,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  autoCompleteContainer: {
    position: 'absolute',
    top: 22,
    left: 10,
    right: 10,
  },
  textInput: {
    padding: 10,
    width: '90%',
    // backgroundColor: '#eee',
    color: 'gray',
    marginVertical: 5,
    marginLeft: 20,
  },
  circle: {
    width: 10,
    height: 10,
    backgroundColor: '#ffdc4a',
    position: 'absolute',
    top: 20,
    left: 5,
    borderRadius: 5,
  },
  line: {
    width: 1,
    height: 45,
    backgroundColor: '#c4c4c4',
    position: 'absolute',
    top: 40,
    left: 10,
    borderRadius: 5,
  },
  square: {
    top: -20,
    left: 5,
    width: 0,
    height: 0,
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderTopWidth: 10,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#00BCD4',
  },
  buttonstyle: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    marginTop: 10,
    width: 200,
    borderWidth: 2,
    borderRadius: 50,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    // backgroundColor:"#ffdc4a"
    tintColor: '#ffdc4a',
  },
  buttonstyle2: {
    // borderTopLeftRadius: 10,
    // borderBottomLeftRadius: 10,
    // marginTop: 10,
    marginHorizontal: 100,
    width: 200,
    // borderWidth: 2,
    borderRadius: 50,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    // backgroundColor:"#ffdc4a",
    tintColor: '#ffdc4a',
    alignSelf: 'center',
  },
})
