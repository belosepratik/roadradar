import ActionSheet from 'react-native-actions-sheet';
import React, {createRef} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Image,
  TextInput,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  Button,
  Modal,
  View,
  CheckBox,
  ImageBackground,
  ProgressBar,
  FlatList,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Wrapped from '../../Components/Global/Wrapped';
import {FloatingIcons} from '../../Components/Universal/FloatingIcons';

const actionSheetRef = createRef();

const App = ({navigation, route}) => {
  let actionSheet;

  React.useEffect(() => {
    actionSheetRef.current?.setModalVisible();
  }, []);

    const OnBoardingSpeedHandle = () => {
        navigation.navigate('ExploreTabs', { "backScreen": route.name });
    }

  const GoBackScreen = () => {
    navigation.goBack();
  };


    return (
        <Wrapped>

            <FloatingIcons bottomPosition="20%" navigation={navigation} onHandle={GoBackScreen} icon="speed" />
            <FloatingIcons bottomPosition="30%" navigation={navigation} onHandle={OnBoardingSpeedHandle} icon="globe" />
            <MapView
                style={{ flex: 1 }}
                showsUserLocation={true}
                provider={PROVIDER_GOOGLE}
                initialRegion={{
                    latitude: 19.2068505,
                    longitude: 72.9528205,
                    latitudeDelta: 0.0222,
                    longitudeDelta: 0.0121,
                }}></MapView>

            {/* <TouchableOpacity
                onPress={() => {
                    actionSheetRef.current?.setModalVisible();
                }}>
                <Text>Open ActionSheet</Text>
            </TouchableOpacity> */}

            <ActionSheet ref={actionSheetRef}>
                <View>
                    <View
                        style={{
                            backgroundColor: 'white',
                            height: 100,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginBottom: -40,
                            borderTopRightRadius: 10,
                            borderTopLeftRadius: 10
                        }}>
                        <Text
                            style={{
                                textAlign: 'left',
                                fontSize: 40,
                                color: 'green',
                                marginLeft: 30,
                                fontWeight: 'bold',
                            }}>
                            40
                        </Text>
                        <Text
                            style={{
                                textAlign: 'left',
                                fontSize: 40,
                                color: 'black',
                                marginRight: 30,
                                fontWeight: 'bold',
                            }}>
                            50
                        </Text>
                    </View>
                    <View
                        style={{
                            backgroundColor: 'white',
                            height: 230,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                        <View>
                            <Text
                                style={{
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'black',
                                    marginLeft: 30,
                                    fontWeight: 'bold',
                                }}>
                                km/hr
                            </Text>
                            <Text
                                style={{
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'black',
                                    margin: -10,
                                    margin: 10,
                                }}>
                                Present Speed
                            </Text>
                        </View>

                        <View>
                            <Text
                                style={{
                                    textAlign: 'right',
                                    fontSize: 18,
                                    color: 'black',
                                    marginBottom: 10,
                                    marginRight: 30,
                                    fontWeight: 'bold',
                                }}>
                                km/hr
                            </Text>
                            <Text
                                style={{
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'black',
                                    marginBottom: 10,
                                    marginRight: 10,
                                }}>
                                Speed Limit
                            </Text>
                        </View>
                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            marginTop: -300,
                            marginBottom: -50,
                        }}>
                        <Image
                            source={require('../../assets/icons/smeter-outer.jpeg')}
                            style={{
                                height: 180,
                                width: 180,
                                margin: 15,
                                position: 'relative',
                                top: -100,
                                left: 55,
                            }}
                        />
                        <Image
                            source={require('../../assets/icons/smeter-outer.jpeg')}
                            style={{
                                height: 80,
                                width: 80,
                                margin: 15,
                                position: 'relative',
                                top: -50,
                                left: -106,
                            }}
                        />
                    </View>
                </View>
            </ActionSheet>

        </Wrapped>);
};

export default App;
