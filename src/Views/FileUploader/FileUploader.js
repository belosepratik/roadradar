import React from 'react';

import {StyleSheet, Text, Image, TouchableOpacity, View} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/Ionicons';

function FileUploader() {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{height: '15%', alignSelf: 'flex-end', padding: 10}}>
        <Icon name="close-outline" size={25} />
      </TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          height: '85%',
          width: '90%',
          alignItems: 'center',
          justifyContent: 'space-around',
        }}>
        <TouchableOpacity style={{alignItems: 'center'}}>
          <Image
            source={{
              uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXMcrXYcN_ZDu-Uc63N-dRKp6yzqufn8LqSg&usqp=CAU',
            }}
            style={{height: 100, width: 100}}
          />
          <Text>Camera</Text>
        </TouchableOpacity>
        <View style={{height: '40%', width: 1, backgroundColor: '#808080'}} />
        <TouchableOpacity style={{alignItems: 'center'}}>
          <Image
            source={{
              uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRFQ9IuIuGCJDWsSfpXiUu77wESS2s4h9ZtA&usqp=CAU',
            }}
            style={{height: 100, width: 100}}
          />
          <Text>Gallery</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'flex-end',
    padding: 5,
    height: responsiveHeight(30),
    width: responsiveWidth(100),
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    alignItems: 'center',
    backgroundColor: 'white', //#6B8E23
  },
});

export default FileUploader;
