import React, { useState } from 'react';
import {
  Image,
  View,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native';
import Header from '../../Components/Header';
import styles from '../../Components/Styles/Styles';
import ProfileBox from '../../Components/AppComponents/ProfileComponents/ProfileBox';
import { useIsFocused } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SettingsList from '../../Components/AppComponents/ProfileComponents/SettingList';
import { NavigationHelpersContext } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { getAllData } from '../../Axios/axios';
import VehicleBox from '../../Components/AppComponents/CategoryComponents/VehicleBox';

import DriverBox from '../../Components/AppComponents/CategoryComponents/DriverBox';

function Category({ navigation, route }) {
  const dispatch = useDispatch();

  const [vehicleData, setVehicleData] = useState();
  const [driverData, setDriverData] = useState();

  const isFocused = useIsFocused();
  React.useEffect(() => {
    (async () => {
      if (isFocused) {
        const res = await dispatch(getAllData('vehicle'));
        console.log(res.data);
        setVehicleData(res.data.results);
        const res1 = await dispatch(getAllData('driver'));
        console.log(res1.data);
        setDriverData(res1.data.results);
      }
    })();
  }, [isFocused]);

  // console.log(vehicleData, 'Hello RR');

  // console.log(driverData, 'Helloww RR');

  return (
    <SafeAreaView style={styles.container}>
      {/* <Header title="Home" navigation={navigation} noFab={true} route={route}> */}
      <ScrollView>
        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              margin: 20,
            }}>
            <Text style={{ fontSize: 24, fontWeight: 'bold' }}>Driver</Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('CategoryStacks', {
                  screen: 'AddDriverScreen',
                });
              }}>
              <MaterialCommunityIcons
                name="plus-circle-outline"
                size={34}
                color="grey"
                style={{ paddingRight: 10 }}
              />
            </TouchableOpacity>
          </View>
          {/* 
          <ProfileBox
            fname="John"
            lname="Doe"
            law="Driver"
            address="Maharashtra Mumbai"
            email="abc@gmail.com"
            phone="+91-9856231452"
          /> */}
          <View>{driverData && <DriverBox driverData={driverData} />}</View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              margin: 20,
            }}>
            <Text style={{ fontSize: 24, fontWeight: 'bold' }}>Vehicle</Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('CategoryStacks', {
                  screen: 'AddVehicleScreen',
                });
              }}>
              <MaterialCommunityIcons
                name="plus-circle-outline"
                size={34}
                color="grey"
                style={{ paddingRight: 10 }}
              />
            </TouchableOpacity>
          </View>
          <View>{vehicleData && <VehicleBox vehicleData={vehicleData} />}</View>
        </View>
      </ScrollView>
      {/* </Header> */}
    </SafeAreaView>
  );
}
export default Category;
