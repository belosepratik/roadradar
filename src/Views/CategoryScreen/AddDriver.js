import React from 'react';
import {
  Image,
  View,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native';
import Header from '../../Components/Header';
// import styles from '../../Components/Styles/Styles';
import ProfileBox from '../../Components/AppComponents/ProfileComponents/ProfileBox';

import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SettingsList from '../../Components/AppComponents/ProfileComponents/SettingList';
import {NavigationHelpersContext} from '@react-navigation/native';
import {useState} from 'react';
import {useDispatch} from 'react-redux';
import { createData } from '../../Axios/axios';


function AddDriver({navigation, route}) {
  const dispatch = useDispatch();

  const handleClick = async () => {
    const res = await dispatch(createData(formData, 'driver'));
    console.log(res);
    // history.push('/home');
    

    {formData && alert("Sucessfully Added")}
    navigation.goBack()
    
    
  };

  // const addVeh=()=>{
  //   alert("Added")
  // }

  const [formData, setFormData] = useState({
    name: '',
    address: '',
    mob_no: '',
    vehicle_name: '',
  });

  console.log(formData);
  return (
    <SafeAreaView style={styles.container}>
      {/* <Header title="Home" navigation={navigation} noFab={true} route={route}> */}
      <ScrollView>
        <View style={{alignItems: 'center', backgroundColor: '#ffdc4a'}}>
          <Text style={{fontSize: 20, paddingTop: 50}}>Add Driver</Text>
          <View style={styles.header}>
            {/* <TouchableOpacity onPress={() => setVisible(false)}>
              <FontAwesome5Icon name="times" size={20} />
            </TouchableOpacity> */}
          </View>
        </View>

        <TextInput
          style={{marginTop: 30, ...styles.txtInputStyle}}
          onChangeText={(text) => setFormData({...formData, name: text})}
          placeholder="Driver Name"></TextInput>
        <TextInput
          style={styles.txtInputStyle}
          onChangeText={(text) => setFormData({...formData, address: text})}
          placeholder="Address"></TextInput>
        <TextInput
          style={styles.txtInputStyle}
          onChangeText={(text) => setFormData({...formData, mob_no: text})}
          placeholder="Mobile Number"></TextInput>
        <TextInput
          style={styles.txtInputStyle}
          onChangeText={(text) => setFormData({...formData, vehicle_name: text})}
          placeholder="Vehicle Name"></TextInput>

        <View
          style={{
            flexDirection: 'row',
            marginVertical: 30,
            marginHorizontal: 5,
            justifyContent: 'space-around',
          }}>
          <TouchableOpacity
            onPress={handleClick}
            style={{
              backgroundColor: '#ffdc4a',
              borderRadius: 25,
              paddingHorizontal: 60,
              paddingVertical: 10,
            }}>
            <Text style={{color: 'black'}}>Add</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      {/* </Header> */}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    fontSize: 42,
  },
  card: {
    padding: 5,
    margin: 10,
    borderRadius: 10,
    elevation: 15,
    backgroundColor: '#fff',
    marginHorizontal: 20,
  },
  img: {
    backgroundColor: 'white',
    borderRadius: 25,
    padding: 10,
    borderColor: 'rgb(37, 150, 190)',
    borderWidth: 3,
    marginTop: 20,
    marginLeft: 20,
  },
  info: {
    flexDirection: 'row',
    padding: 10,
  },
  modalBackGround: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    width: '80%',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
    elevation: 20,
    height: '80%',
  },
  header: {
    width: '100%',
    height: 40,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  txtInputStyle: {
    fontSize: 20,
    margin: 10,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderColor: '#ffdc4a',
    color: 'gray',
  },
});

export default AddDriver;
