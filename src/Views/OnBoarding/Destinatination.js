import React from 'react';
import { View } from 'react-native';
import OnBoardingDest from '../../Components/AppComponents/OnBoardingComponent/OnBoardingDest';
import Wrapped from '../../Components/Global/Wrapped';
import { windowHeight, windowWidth } from '../../Util/Dimensions';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { useSelector } from 'react-redux';

export default function Destinatination({ navigation, route }) {

  console.log("Destination Current >>>>>>>>>>>", route.name)
  console.log("Destination Prev >>>>>>>>>>>", route.params.backScreen)

  const home = useSelector(state => state.home)
  const { flow } = home

  console.log("flow >>>>>>>>>>>", flow)

  const HandleBoarding = () => {
    switch (flow) {
      case "Drive":
        navigation.navigate("OnCarpoolAdd", { "backScreen": route.name });
        break;
      case "Schedule Drive":
        navigation.navigate("ScheduleForLater", { "backScreen": route.name });
        break;
      case "Ride":
        navigation.navigate("OnCarpoolAdd", { "backScreen": route.name });
        break;
      case "Schedule Ride":
        navigation.navigate("ScheduleForLater", { "backScreen": route.name });
        break;
    }
  };

  return (

    <View
      style={{
        position: 'absolute',
        width: windowWidth,
        height: windowHeight,
      }}>
      <View style={{ height: windowHeight, justifyContent: 'flex-end' }}>
        <OnBoardingDest
          HandleBoarding={HandleBoarding}
          navigation={navigation}
        />
      </View>
    </View>
  );
}
