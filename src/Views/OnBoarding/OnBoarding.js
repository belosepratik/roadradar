import React, { useState } from 'react';
import { Text, View } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import HomeAbsoluteCopmponents from '../../Components/AppComponents/HomeComponents/HomeAbsoluteCopmponents';
import OnBoardingsilide1 from '../../Components/AppComponents/OnBoardingComponent/OnBoardingsilide1';
import Wrapped from '../../Components/Basic/Wrapped';
import { windowHeight, windowWidth } from '../../Util/Dimensions';
import OnBoardingsilide2 from '../../Components/AppComponents/OnBoardingComponent/OnBoardingsilide2';
import OnBoardingsilide3 from '../../Components/AppComponents/OnBoardingComponent/OnBoardingsilide3';
import OnBoardingDest from '../../Components/AppComponents/OnBoardingComponent/OnBoardingDest';
import OnBoardingSpeed from '../../Components/AppComponents/OnBoardingComponent/OnBoardingSpeed';
import OnBoardingSpeedIndicator from '../../Components/AppComponents/OnBoardingComponent/OnBoardingSpeedIndicator';
import { useDispatch } from 'react-redux';
import { setFlow, setOnBoardingStatus } from '../../Redux/Slice/HomeSlice';

export default function OnBoarding({ navigation, route }) {

  console.log("onBoarding route>>>>>>>>>>", route.name)

  const dispatch = useDispatch();

  const [boarding, setboarding] = useState(0);

  const [oboarding1, setoboarding1] = useState(true);

  const HandleBoarding2 = () => {
    setboarding(2);
  };

  const HandleBoarding3 = () => {
    setboarding(3);
  };

  const HandleBoarding4 = (value) => {
    setboarding(value);
  };

  const HandleHome = () => {
    navigation.replace('Home');
  };

  const HandleLeft = () => {
    setoboarding1(!oboarding1);
    dispatch(setOnBoardingStatus(oboarding1));
    if (oboarding1) {
      dispatch(setFlow('Ride'));
    } else {
      dispatch(setFlow('Drive'));
    }
  };

  return (
    <Wrapped>
      <MapView
        style={{ flex: 1 }}
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 19.2068505,
          longitude: 72.9528205,
          latitudeDelta: 0.0222,
          longitudeDelta: 0.0121,
        }}></MapView>
      <View
        style={{
          position: 'absolute',
          width: windowWidth,
          height: windowHeight,
        }}>
        <View style={{ height: windowHeight, justifyContent: 'flex-end' }}>
          {
            boarding == 0 && (
              <OnBoardingsilide1
                HandleLeft={HandleLeft}
                oboarding1={oboarding1}
                navigation={navigation}
                backScreen={route.name}
              />
            )
            // :
            // <>
            //     {
            //         boarding == 1 ?
            //             <OnBoardingDest HandleBoarding={HandleBoarding2} />
            //             :
            //             <>
            //                 {boarding == 2 ? <OnBoardingsilide2 HandleBoarding={HandleBoarding3} />
            //                     :
            //                     <>
            //                         {boarding == 3 ?
            //                             <OnBoardingSpeed HandleBoarding={HandleBoarding4} boarding={boarding} />
            //                             :
            //                             <>
            //                                 {
            //                                     boarding == 4 ?
            //                                         <OnBoardingSpeedIndicator HandleBoarding4={HandleBoarding4} />
            //                                         :
            //                                         <OnBoardingsilide3 HandleBoarding={HandleHome} navigation={navigation} />

            //                                 }
            //                             </>

            //                         }
            //                     </>

            //                 }
            //             </>
            //     }

            // </>
          }
        </View>
      </View>
    </Wrapped>
  );
}
