import React, {useEffect, useState, createRef} from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
import {Provider} from 'react-redux';
import {store, persistor} from '../../../src/Redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import {persistStore} from 'redux-persist';
import Navigation from '../../../src/Routes/Navigation';
import {View, Text, Image} from 'react-native';
import Fn from 'react-native-vector-icons/FontAwesome5';
import HomeIcon from '../../../src/assets/IconComponent/HomeIcon';
import Ant from 'react-native-vector-icons/AntDesign';
import {MAP_KEY} from '../.././../Config';
import {Button, Footer, FooterTab, Icon, Thumbnail, Badge} from 'native-base';
import OnlineIcon from '../../../src/assets/IconComponent/OnlineIcon';
import OfflineIcon from '../../../src/assets/IconComponent/OfflineIcon';
import {grey} from 'ansi-colors';
import {TouchableOpacity} from 'react-native';
import ActionSheet from 'react-native-actions-sheet';
import {Calendar, LocaleConfig} from 'react-native-calendars';
import MapViewDirections from 'react-native-maps-directions';
import Wrapped from '../../../src/Components/Global/Wrapped';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {windowWidth} from '../../../src/Util/Dimensions';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import { BackButton } from '../../Components/Universal/BackButton';
// import Icon from 'react-native-vector-icons/MaterialIcons';

const actionSheetRef = createRef();
const ScheduleForLater = ({navigation, route}) => {
  console.log('ScheduleForLater Current >>>>>>>>>>>', route.name);
  console.log('ScheduleForLater Prev >>>>>>>>>>>', route.params.backScreen);

  const [showButton, setShowButton] = React.useState(false);
  let actionSheet;
  const homePlace = {
    description: 'Home',
    geometry: {location: {lat: 48.8152937, lng: 2.4597668}},
  };
  const workPlace = {
    description: 'Work',
    geometry: {location: {lat: 48.8496818, lng: 2.2940881}},
  };
  const wifi1 = <HomeIcon fill={'black'} width={30} height={30} />;
  const off = <OfflineIcon fill={'black'} width={30} height={30} />;
  const wifi = <OnlineIcon fill={'black'} width={30} height={30} />;
  const tomorrow = {date: 23, month: 'july', year: 'April'};
  LocaleConfig.locales['fr'] = {
    monthNames: [
      'Jan',
      'Feb',
      'Mar',
      'April',
      'May',
      'June',
      'July',
      'Aug',
      'Sept',
      'October',
      'Nov',
      'Dec',
    ],
    monthNamesShort: [
      'Jan',
      'Feb',
      'Mar',
      'April',
      'May',
      'June',
      'July',
      'Aug',
      'Sept',
      'October',
      'Nov',
      'Dec',
    ],
    dayNames: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    today: "Aujourd'hui",
  };
  const [origin, setorigin] = useState();
  LocaleConfig.defaultLocale = 'fr';
  const from = {address: 'thane west'}; //////location from
  const to = {address: 'mulund west'}; //////location to

  useEffect(() => {
    actionSheetRef.current?.setModalVisible();
  }, []);

  return (
    <Wrapped>
      <BackButton navigation={navigation} route={route} />
      <MapView
        style={{flex: 1}}
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 19.2068505,
          longitude: 72.9528205,
          latitudeDelta: 0.0222,
          longitudeDelta: 0.0121,
        }}></MapView>

      <View
        style={{
          justifyContent: 'flex-start',
          flex: 1,
        }}>
        <ActionSheet
          ref={actionSheetRef}
          containerStyle={{backgroundColor: 'white', borderRadius: 35}}>
          <View
            style={{backgroundColor: 'white', borderRadius: 35, padding: 20}}>
            <Text
              style={{fontSize: responsiveFontSize(2.8), fontWeight: '900'}}>
              Schedule for later
            </Text>
            <Text style={{color: 'grey', fontSize: responsiveFontSize(2.8)}}>
              Choose date
            </Text>
            <View>
              <Calendar>
                // Initially visible month. Default = Date() current=
                {'2012-03-01'}
                // Minimum date that can be selected, dates before minDate will
                be grayed out. Default = undefined minDate={'2012-05-10'}
                // Maximum date that can be selected, dates after maxDate will
                be grayed out. Default = undefined maxDate={'2012-05-30'}
                // Handler which gets executed on day press. Default = undefined
                onDayPress=
                {(day) => {
                  console.log('selected day', day);
                }}
                // Handler which gets executed on day long press. Default =
                undefined onDayLongPress=
                {(day) => {
                  console.log('selected day', day);
                }}
                // Month format in calendar title. Formatting values:
                http://arshaw.com/xdate/#Formatting monthFormat={'yyyy MM'}
                // Handler which gets executed when visible month changes in
                calendar. Default = undefined onMonthChange=
                {(month) => {
                  console.log('month changed', month);
                }}
                // Hide month navigation arrows. Default = false hideArrows=
                {true}
                // Replace default arrows with custom ones (direction can be
                'left' or 'right') renderArrow={(direction) => <Arrow />}
                // Do not show days of other months in month page. Default =
                false hideExtraDays={true}
                // If hideArrows = false and hideExtraDays = false do not switch
                month when tapping on greyed out // day from another month that
                is visible in calendar page. Default = false disableMonthChange=
                {true}
                // If firstDay=1 week starts from Monday. Note that dayNames and
                dayNamesShort should still start from Sunday firstDay={1}
                // Hide day names. Default = false hideDayNames={true}
                // Show week numbers to the left. Default = false
                showWeekNumbers=
                {true}
                // Handler which gets executed when press arrow icon left. It
                receive a callback can go back month onPressArrowLeft=
                {(subtractMonth) => subtractMonth()}
                // Handler which gets executed when press arrow icon right. It
                receive a callback can go next month onPressArrowRight=
                {(addMonth) => addMonth()}
                // Disable left arrow. Default = false disableArrowLeft={true}
                // Disable right arrow. Default = false disableArrowRight={true}
                // Disable all touch events for disabled days. can be override
                with disableTouchEvent in markedDates
                disableAllTouchEventsForDisabledDays={true}
                // Replace default month and year title with custom one. the
                function receive a date as parameter renderHeader=
                {(date) => {
                  /*Return JSX*/
                }}
                // Enable the option to swipe between months. Default = false
                enableSwipeMonths={true}
              </Calendar>
            </View>
            <View
              style={{
                justifyContent: 'flex-start',
                flexDirection: 'row',
                padding: 30,
                alignItems: 'center',
              }}>
              <Text style={{fontSize: responsiveFontSize(2.3)}}>
                {/* <OnlineIcon fill={'blue'} width={20} height={18} /> */}
                <Fn name="circle" color="#1E8FE7" size={19} />
                Tomorrow -{' '}
              </Text>
              <Text style={{fontSize: responsiveFontSize(2.3), color: 'grey'}}>
                {tomorrow.date}
                {tomorrow.month}
                {tomorrow.year}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-between',
                marginLeft: 30,
              }}>
              <View
                style={{justifyContent: 'flex-start', flexDirection: 'column'}}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2.3),
                    flexDirection: 'row',
                  }}>
                  <Fn name="dot-circle" size={25} color="red" /> {from.address}{' '}
                </Text>

                <Fn
                  name="ellipsis-v"
                  size={20}
                  color="black"
                  style={{alignSelf: 'flex-start', marginLeft: 5}}
                />
                <Text
                  style={{
                    fontSize: responsiveFontSize(2.3),
                    flexDirection: 'row',
                  }}>
                  <Fn name="map-marker-alt" size={28} color="#1E8FE7" />{' '}
                  {to.address}{' '}
                </Text>
              </View>
            </View>
            <View
              style={{marginTop: 20, alignItems: 'center', marginBottom: -170}}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('SFLTimer', {backScreen: route.name});
                  actionSheetRef.current?.hide();
                }}
                style={{
                  backgroundColor: '#1E8FE7',
                  padding: 10,
                  borderRadius: 10,
                  height: '30%',
                  width: '90%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: responsiveFontSize(2.8),
                    marginLeft: 25,
                  }}>
                  Continue
                </Text>
                <Fn
                  name="arrow-right"
                  style={{color: 'white', marginRight: 30}}
                  size={30}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ActionSheet>
      </View>
    </Wrapped>
  );
};

export default ScheduleForLater;
