import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import {
    responsiveScreenHeight as rH,
    responsiveScreenWidth as rW,
    responsiveScreenFontSize as rS,

} from "react-native-responsive-dimensions";
import Icon from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
export default function CarImage() {
    return (

        <View style={{ display: "flex", flexDirection: "row", paddingTop: 20 }}>
            <View
                style={{
                    display: "flex",
                    flexDirection: "row",
                    //    borderWidth: 1,
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <View style={{ height: rH(5), width: rW(16), backgroundColor: '#fff', borderRadius: 5, borderWidth: 1, borderColor: 'green', justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons name="car" size={45} color="black" />
                </View>


            </View>
            <View
                style={{
                    flex: 1,
                    flexDirection: "row",
                    marginTop: 5,
                    //   borderWidth: 1,
                    marginLeft: 20,
                    alignItems: "center",
                    justifyContent: "space-around",
                }}
            >
                <View>
                    <Text style={{ fontSize: 20, fontWeight: "600" }}>
                        car-image.jpg
                    </Text>
                    <Text style={{ fontSize: 15, color: "grey" }}>64kb</Text>
                </View>
                <View>
                    <View style={{ width: rW(40), alignItems: 'flex-end', justifyContent: 'space-between', paddingRight: 60 }}>
                        <Icon name="delete" size={30} color="tomato"></Icon>
                    </View>
                </View>
            </View>
        </View>

    )
}
