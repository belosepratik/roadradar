import React from 'react'
import { View, Text, StyleSheet, TextInput } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
    responsiveScreenHeight as rH,
    responsiveScreenWidth as rW,
    responsiveScreenFontSize as rS,
} from "react-native-responsive-dimensions";

import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import ChooseFile from './ChooseFile'
import CarImage from './CarImage'
import Continue from './Continue'

export default function VehicleAdd() {
    return (
        <TouchableOpacity
            disabled={true}
            style={StyleSheet.container}
        >

            <View style={styles.modal}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <View style={{ flex: .13, flexDirection: 'row', justifyContent: 'flex-start' }}>
                        <Text style={styles.text, {
                            // flex:1,
                            alignItems: 'flex-end',
                            justifyContent: 'space-between',
                            fontSize: 34,
                            width: rW(50),
                            height: rH(5),
                            marginRight: 30,
                            fontWeight: 'bold',
                        }}>Add Vehicle</Text>

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', height: rH(3.5), marginTop: 10, paddingRight: 60 }}>
                            <Ionicons name="chevron-back-outline" size={30} color="grey" />
                            <Text style={{ fontSize: 18, color: 'grey', paddingTop: 3 }}>Back</Text>
                        </View>

                    </View>


                    <View style={{ display: 'flex', paddingTop: 50 }}>
                        <Text style={{ fontSize: 22, fontWeight: '600' }}>About the vehicle</Text>
                    </View>

                    <View style={{ flex: .66, flexDirection: 'column', width: rW(80) }}>

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', marginTop: 10, paddingRight: 60, borderBottomWidth: 1, width: rW(77), borderColor: 'grey' }}>
                            <MaterialIcons name="confirmation-number" size={30} color="grey" />
                            <TextInput style={{ backgroundColor: '#FFF', height: rH(3), width: rW(71), paddingTop: 10 }} placeholder="Vehicle number" style={{ paddingLeft: 20, fontSize: 20 }} />
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 20, paddingRight: 60, borderBottomWidth: 1, width: rW(77), borderColor: 'grey' }}>
                            <MaterialIcons name="invert-colors" size={30} color="grey" />
                            <TextInput style={{ backgroundColor: '#FFF', height: rH(3), width: rW(71) }} placeholder="Vehicle colour" style={{ paddingLeft: 20, fontSize: 20 }} />
                        </View>
                    </View>

                    <View style={{ marginTop: 30 }}>
                        <Text style={{ fontSize: 24 }}>Upload Image</Text>
                    </View>

                    <View style={{ width: rW(80) }}>
                        <View style={{ display: 'flex', marginTop: 20, height: rH(20), borderStyle: 'dashed', borderColor: 'grey', flexDirection: 'column', borderWidth: 2.2, borderRadius: 20, width: rW(76), paddingBottom: 100, justifyContent: 'center', alignItems: 'center' }}>
                            <ChooseFile />
                        </View>


                    </View>

                    <View style={{display:'flex'}}>
                    <CarImage />
                    </View>

                    <View style={{ paddingTop: 10 }}>
                        <CarImage />
                    </View>
                    <View style={{ justifyContent: 'flex-end', paddingTop: 100 }}>
                        <Continue />
                    </View>




                </View>
                <View style={styles.buttonsView}>

                    <TouchableOpacity
                        style={styles.TouchableOpacity}
                        onPress={() => CloseModal(false, 'Ok')}
                    >
                        <Text style={styles.text}>Close</Text>

                    </TouchableOpacity>
                </View>
            </View>

        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    modal: {
        height: rH(100),
        width: rW(100),
        paddingTop: 30,
        paddingLeft: 40,
        backgroundColor: '#fff',

    },
    text: {
        display: 'flex',
        fontSize: 18,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        fontWeight: '600',
        marginRight: 30,
    },
    buttonsView: {
        flex: .1,
        height: rH(2),
        width: rW(90),
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

    },
    TouchableOpacity: {
        flex: 1,
        paddingVertical: 10,
        alignItems: 'flex-end',
    }

});
