import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Platform, Button, StatusBar } from "react-native";
import * as ImagePicker from 'react-native-image-picker'
import { FontAwesome } from "react-native-vector-icons/FontAwesome";
// import { MdCloudUpload } from 'react-icons/md';
import Icon from "react-native-vector-icons/MaterialIcons"; 
// import { cond } from "lodash";
function Start(props) {
    const [image, setImage] = useState(null);

    // useEffect(async () => {
    //     if (Platform.OS !== 'web') {
    //         const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
    //         if (status !== 'granted') {
    //             alert('Permission denied!')
    //         }
    //     }

    // }, [])

    const PickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1
        })
        console.log(result)
        if (!result.cancelled) {
            setImage(result.uri)
        }
    }
    // let openImage = async () => {
    //     let permission = await ImagePicker.requestCameraPermissionsAsync

    //     if(permission.granted === false) {
    //         return;
    //     }

    //     let picker = await ImagePicker.launchImageLibraryAsync()

    //     console.log(picker)

    // }


    return (
        <View style={styles.container}>

            {/* <Button title="choose Image" onPress={PickImage} />
            {image && <Image source={{ uri: image }} style={{
                width: 200, 
                height: 200,
                justifyContent:'center', 
                alignItems:'center',
                
            }} />} */}
            
            <View style={styles.Upload}>
            
                <Image />
                
                <TouchableOpacity

                    onPress={PickImage}


                    style={styles.button}>
                    {/* <Icon name="MdCloudUpload"/> */}
                    {/* <FontAwesome name="cloud-upload" size={80} color="grey" /> */}
                    <Icon name="cloud-upload" size={80} color="gray"></Icon>
                {image && <Image source={{ uri: image }} style={{ color: '#000', width:200, height:200 }} />}
                </TouchableOpacity>
                
                <Text style={styles.text}>Tap to choose</Text>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // borderWidth:1,
        paddingTop: 70
    },

    Upload: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        // borderWidth:1
    },

    button: {
        display: 'flex',
        backgroundColor: 'cyan',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 18,
        fontWeight: '600',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40,
        color: 'grey',
        // borderWidth:1
    }
});
export default Start;