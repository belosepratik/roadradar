import React from "react";
import { View, Text, StyleSheet } from "react-native";
import {
  responsiveHeight as rH,
  responsiveWidth as rW,
  responsiveFontSize as rS,
} from "react-native-responsive-dimensions";
import FontAwesome from "react-native-vector-icons/FontAwesome";

function Continue() {
  return (
    <View style={styles.container}>
      <View style={{height:rH(9), width:rW(80),borderRadius:15, flexDirection:'row',justifyContent:'space-between',alignItems:'center',  backgroundColor:'#1d91ca', marginTop:30}}>
            <Text style={{display:'flex', flexDirection:'row',fontSize:24, alignItems:'center', justifyContent:'center', paddingLeft:40, color:'#FFF'}}> Continue</Text>
            <FontAwesome name="long-arrow-right" size={35} color="white" 
            style={{paddingRight:40}} />
            </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    display:'flex'
  },
});
export default Continue;