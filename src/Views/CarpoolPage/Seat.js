import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {
  responsiveScreenHeight as rH,
  responsiveScreenWidth as rW,
  responsiveScreenFontSize as rS,
} from 'react-native-responsive-dimensions';

export default function Seat() {
  return (
    <View>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={require('../../assets/seat.png')}
          style={{
            display: 'flex',
            height: rH(9),
            width: rW(15),
            //  paddingLeft: 20,
          }}
        />
      </View>
    </View>
  );
}
