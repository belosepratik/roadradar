import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {
  responsiveHeight as rH,
  responsiveWidth as rW,
  responsiveFontSize as rS,
} from 'react-native-responsive-dimensions';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {useSelector} from 'react-redux';

function Start({navigation, handleCarpoolAdd}) {
  const Home = useSelector((state) => state.home);
  const {onboardingStatus, flow} = Home;

  // console.log('onboardingStatus>>>>>>', onboardingStatus);
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => {
          handleCarpoolAdd();
        }}>
        <View
          style={{
            height: rH(10),
            width: rW(85),
            borderRadius: 15,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: '#1d91ca',
            marginTop: 30,
          }}>
          <Text
            style={{
              display: 'flex',
              flexDirection: 'row',
              fontSize: 24,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: 30,
              color: '#FFF',
            }}>
            {flow == 'Drive' || flow == 'Ride' ? 'Start' : 'Finish'}
          </Text>
          <FontAwesome
            name="long-arrow-right"
            size={30}
            color="white"
            style={{marginRight: 20}}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    display: 'flex',
  },
});
export default Start;

{
  /* <Button
        title="finish"
        // onPress={() => Alert.alert('Simple Button pressed')}
        style={{}}
      /> */
}
