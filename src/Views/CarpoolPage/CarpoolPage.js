import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Switch,
  Image,
} from 'react-native';
import {
  responsiveScreenHeight as rH,
  responsiveScreenWidth as rW,
  responsiveScreenFontSize as rS,
} from 'react-native-responsive-dimensions';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Start from './Start';
import Finish from './Finish';
import Carpool from './Carpool';
// import BackButton from './BackButton';
import Seat from './Seat';
import Backarrow from 'react-native-vector-icons/Ionicons';
import Wrapped from '../../Components/Global/Wrapped';

import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {useSelector} from 'react-redux';
import {BackButton} from '../../Components/Universal/BackButton';

export default function CarpoolPage({navigation, route}) {
  const [isEnabled, setIsEnabled] = useState(false);

  console.log('CarpoolPage Current >>>>>>>>>>>', route.name);
  console.log('CarpoolPage Prev >>>>>>>>>>>', route.params.backScreen);

  const home = useSelector((state) => state.home);
  const {flow} = home;

  const HandleCarpoolAdd = () => {
    if (flow == 'Drive') {
      navigation.navigate('OnSpeed', {backScreen: route.name});
    } else if (flow == 'Schedule Drive') {
      console.log('Schedule Drive End');
    }

    switch (flow) {
      case 'Drive':
        navigation.navigate('OnSpeed', {backScreen: route.name});
        break;
      case 'Schedule Drive':
        // navigation.navigate("ScheduleForLater", { "backScreen": route.name });
        console.log('Schedule Drive End');
        break;
      case 'Ride':
        navigation.navigate('Carpool', {backScreen: route.name});
        break;
      case 'Ride Schedule':
        navigation.navigate('OnDestinatination', {backScreen: route.name});
        break;
    }
  };

  return (
    <Wrapped>
      <BackButton navigation={navigation} route={route} />
      <MapView
        style={{flex: 1}}
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 19.2068505,
          longitude: 72.9528205,
          latitudeDelta: 0.0222,
          longitudeDelta: 0.0121,
        }}></MapView>
      <TouchableOpacity disabled={true} style={StyleSheet.container}>
        {/* <View style={{paddingLeft: 25, paddingTop: 20}}>
          <BackButton />
        </View> */}

        <View style={styles.modal}>
          {/* <View
            style={{flex: 1, backgroundColor: 'red', flexDirection: 'column'}}> */}
          <Text
            style={
              (styles.text,
              {
                //  display: 'flex',
                //alignItems: 'flex-start',
                // justifyContent: 'flex-start',
                fontSize: rS(3),
                fontFamily: 'raleway',
                // marginRight: 20,
                fontWeight: '600',
                //marginTop: 30,
              })
            }>
            Add carpool
          </Text>

          <Text style={{fontSize: rS(2), color: '#565656'}}>
            Select if you want carpool
          </Text>

          <View
            style={{
              height: rH(10),
              flexDirection: 'column',
              marginTop: 20,
            }}>
            <Carpool isEnabled={isEnabled} setIsEnabled={setIsEnabled} />
          </View>
          <View style={{}}>
            {isEnabled && (
              <>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '90%',
                    paddingTop: 16,
                  }}>
                  <Text style={{fontSize: 16, fontWeight: '400'}}>
                    Select the number of seats
                  </Text>
                  <Text style={{fontSize: 16, color: 'red'}}>(1 selected)</Text>
                </View>
                <View
                  style={{
                    display: 'flex',
                    marginRight: 50,
                    paddingTop: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    // alignItems: 'flex-start',
                  }}>
                  <Seat />
                  <Seat />
                  <Seat />
                  <Seat />
                </View>
              </>
            )}

            <Finish
              navigation={navigation}
              handleCarpoolAdd={HandleCarpoolAdd}
            />
          </View>
        </View>
        {/* </View> */}
      </TouchableOpacity>
    </Wrapped>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'cyan',
    alignItems: 'center',
    justifyContent: 'center',
  },

  modal: {
    // marginTop: 270,
    height: rH(60),
    width: rW(100),
    paddingTop: 30,
    paddingLeft: 30,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    //borderWidth: 1,
    elevation: 5,
  },
  Box: {
    flex: 1,
    display: 'flex',
    width: rW(15),
    height: rH(3),
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    fontSize: rS(1),
    marginTop: 3,
    borderWidth: 1,
  },
  text: {
    //  display: 'flex',
    fontSize: rS(2),
    // justifyContent: 'flex-end',
    // alignItems: 'flex-end',
    color: 'black',
  },

  TouchableOpacity: {
    flex: 1,
    paddingVertical: 10,
    alignItems: 'center',
  },
});
