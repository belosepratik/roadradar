import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import {
  responsiveHeight as rH,
  responsiveWidth as rW,
  responsiveFontSize as rS,
} from "react-native-responsive-dimensions";  
import Ionicons from 'react-native-vector-icons/Ionicons';


function Start(props) {
  return (
    
    <View style={styles.container}>
    <TouchableOpacity onPress={() => console.log("Start")}>
      <View style={{height:rH(9), width:rW(80),borderRadius:10, flexDirection:'row',justifyContent:'space-between',alignItems:'center',  backgroundColor:'#1d91ca', marginTop:30}}>
            <Text style={{display:'flex', flexDirection:'row',fontSize:24, alignItems:'center', justifyContent:'center', marginLeft:10, color:'#FFF'}}> Start</Text>
            <Ionicons name="long-arrow-right" size={35} color="white" 
            style={{marginRight:20}} />
            </View>
            </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    display:'flex'
  },
});
export default Start;