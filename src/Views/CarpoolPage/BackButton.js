import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Alert } from "react-native";
import {
  responsiveWidth as vw,
} from "react-native-responsive-dimensions";
import Icon from "react-native-vector-icons/MaterialIcons";
function BackButton(props) {
  return (
      <View style={{display:'flex'}}>
      <TouchableOpacity onPress={() => console.log("Back")}>
    <View style={styles.container}>
      <Icon name="arrow-back" size={vw(8.86)} style={styles.icn}></Icon>
    </View>
    </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: 70,
    height: 60,
    backgroundColor: "#fff",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop:10
  },
});
export default BackButton;