import React, {useState} from 'react';
import {View, StyleSheet, Text, Switch, TouchableOpacity} from 'react-native';
import {
  responsiveScreenHeight as rH,
  responsiveScreenWidth as rW,
  responsiveScreenFontSize as rS,
} from 'react-native-responsive-dimensions';
// import Icon from 'react-native-vector-icons/Icon';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ToggleSwitch from 'toggle-switch-react-native';

export default function Carpool({setIsEnabled, isEnabled}) {
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  return (
    <View style={{display: 'flex', flexDirection: 'row'}}>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          //    borderWidth: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            height: rH(9),
            width: rW(16),
            backgroundColor: '#ff4d4d',
            borderRadius: 12,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Ionicons name="car" size={45} color="black" />
        </View>
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          marginTop: 0,
          //   borderWidth: 1,
          marginLeft: 0,
          alignItems: 'center',
          justifyContent: 'space-around',
        }}>
        <View>
          <Text style={{fontSize: 20, fontWeight: '500'}}>Carpool</Text>
          <Text style={{fontSize: 14, color: '#565656'}}>
            4 seats available
          </Text>
        </View>
        <View>
          <View
            style={{
              // width: rW(30),
              alignItems: 'flex-end',
              //backgroundColor: 'yellow',
              justifyContent: 'space-between',
              // paddingRight: 45,
            }}>
            {/* <Switch style={{backgroundColor:'#f5dd4b',width:45,borderWidth:3,borderColor:'#1e91ca'}}
              // trackColor={{false: '#767577', true: '#81b0ff'}}
              // thumbColor={isEnabled ? '#f5dd4b' : '#f4f3f4'}
              trackColor={{false: '#1e91ca', true: '#1e91ca'}}
              thumbColor={isEnabled ? '#f4f3f4' : '#f4f3f4'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            /> */}
            <ToggleSwitch
              isOn={isEnabled}
              onColor="#1e91ca"
              offColor="#1e91ca"
              size="medium"
              labelStyle={{color: 'black', fontWeight: '900'}}
              onToggle={() => toggleSwitch()}
            />
            {/* <Icon name="toggle-on" size={80} color="#1d91ca"></Icon> */}
          </View>
        </View>
      </View>
    </View>
  );
}
