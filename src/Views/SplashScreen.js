import React from 'react';
import {
  Image,
  View,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  Text,
  StyleSheet,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Avatar } from 'react-native-paper';
import LocationEnabler from 'react-native-location-enabler';
import GetLocation from 'react-native-get-location';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import { setloading, setLocation } from '../Redux/Slice/LoginSlice';
import Wrapped from '../Components/Basic/Wrapped';
import { GeoDecoding } from '../functions/GeoDecording';
import { MAP_KEY } from '../../Config';
import { setmyLocation } from '../Redux/Slice/HomeSlice';
import Geolocation from '@react-native-community/geolocation';

const {
  PRIORITIES: { HIGH_ACCURACY },
  useLocationSettings,
} = LocationEnabler;

function SplashScreen({ navigation }) {
  const login = useSelector(state => state.login)
  const { location, loading } = login

  const dispatch = useDispatch()

  const [enabled, requestResolution] = useLocationSettings(
    {
      priority: HIGH_ACCURACY, // default BALANCED_POWER_ACCURACY
      alwaysShow: true, // default false
      needBle: true, // default false
    },
    false /* optional: default undefined */
  );

  // console.log(res)

  // React.useEffect(() => {
  //   RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
  //     interval: 10000,
  //     fastInterval: 5000,
  //   })
  //     .then((data) => {
  //       console.log(data)

  //       GetLocation.getCurrentPosition({
  //         enableHighAccuracy: true,
  //         timeout: 15000,
  //       })
  //         .then(location => {
  //           console.log(location);
  //           GeoDecoding(location.latitude, location.longitude, MAP_KEY)
  //             .then((res) => {
  //               dispatch(setmyLocation(res))
  //             })
  //               dispatch(setLocation(location))


  //             })
  //             .catch(error => {
  //               const { code, message } = error;
  //               dispatch(setLocation([]))

  //             })
  //         })
  //         .catch((err) => {
  //           console.log(err)

  //         });

  //       setTimeout(() => {


  //         navigation.replace('OnBoarding');

  //       }, location ? 3000 : null);



  //     }, [])

  React.useEffect(() => {
    setTimeout(() => {
      (async () => {
        dispatch(setloading(true))
        requestResolution()
        await Geolocation.getCurrentPosition(
          (position) => {
            console.log('Position -> ',);
            dispatch(setLocation(position.coords))
            GeoDecoding(position.coords.latitude, position.coords.longitude, MAP_KEY)
              .then((res) => {
                console.log(res)
                dispatch(setmyLocation(res))
                dispatch(setloading(false))
                navigation.replace('OnBoarding');
              })
          },
          (error) => { dispatch(setloading(false)), console.log(error) },
          { enableHighAccuracy: false, timeout: 50000 }
        );

        dispatch(setloading(false))
      })()
    }, loading == false ? 3000 : null);
  }, [])


  return (
    <Wrapped>
      <View style={{ flex: 1, justifyContent: "center", alignItems: 'center',backgroundColor: "white" }}>
        {/* <Text>Splash Screen</Text> */}
        <Avatar.Image size={300} source={require('./../assets/logo/rr_logo.jpg')} />
      </View>
    </Wrapped>


  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 25,
    padding: 15,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  contentCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: 'white',
    padding: 10,
  },
});
export default SplashScreen;
