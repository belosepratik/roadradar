import React from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Delete from 'react-native-vector-icons/MaterialIcons';
import Add from 'react-native-vector-icons/MaterialIcons';
import Arrow from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function ParkingSpot() {
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          <View>
            <Text
              style={{
                padding: 20,
                fontSize: 25,
                fontWeight: 'bold',
                marginLeft: 9,
                marginTop: 20,
              }}>
              Parking spots
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 20,
            }}>
            <View style={{marginLeft: 30}}>
              <Image
                style={styles.tinyLogo}
                source={require('../../assets/car.jpg')}
              />
            </View>
            <View style={{marginRight: 80}}>
              <Text style={styles.vehicle}>Four Wheeler</Text>
              <Text style={styles.car}>A-15 Cb road | EV</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={{marginRight: 35, alignSelf: 'center'}}>
                <Delete name="delete" size={20} color="red" />
              </Text>
            </View>
          </View>
          <View>
            <View style={{flexDirection: 'row', marginTop: 30}}>
              <Text
                style={{
                  paddingLeft: 20,
                  fontSize: 20,
                  color: 'black',
                  marginLeft: 9,
                }}>
                Add more
              </Text>
              <Text style={{alignSelf: 'center', paddingLeft: 3}}>
                <Add name="add" size={20} color="red" />
              </Text>
            </View>
          </View>
          <View style={{marginTop: 350}} />
          <View>
            <TouchableOpacity style={styles.btn}>
              <Text style={{color: 'white', fontSize: 20}}>Finish</Text>
              <Text>
                <Arrow name="arrow-forward" size={25} color="white" />
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* <StatusBar
          hidden={false}
          backgroundColor="#b3e6ff"
          barStyle="dark-content"
          translucent={false}
        /> */}
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  tinyLogo: {
    width: 70,
    height: 40,
    borderRadius: 4,
  },
  vehicle: {
    fontWeight: 'bold',
  },
  car: {
    color: 'grey',
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '84%',
    borderRadius: 9,
    backgroundColor: '#0099ff',
    marginTop: 20,
    alignSelf: 'center',
    padding: 10,
    paddingLeft: 20,
    paddingRight: 10,
  },
});
