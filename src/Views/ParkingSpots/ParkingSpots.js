import React from "react";

import { StyleSheet, TouchableOpacity, Text, Image, View } from "react-native";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import Icon from "react-native-vector-icons/Ionicons";

function ParkingSpots() {
    return (
        <View style={styles.container}>
            <View style={styles.namecontainer}>
                <Text style={styles.text}>Parking spots</Text>
                {/* <TouchableOpacity style={{flexDirection:'row', height:'100%', alignItems:'center'}}>
        <Text>skip</Text>
        <Icon name="chevron-forward-outline" size={15}/>
      </TouchableOpacity> */}
            </View>
            <View style={styles.midcontainer}>
                <View style={styles.card}>
                    <View style={{ width: "20%" }}>
                        <Image
                            source={{
                                uri:
                                    "https://media.wired.com/photos/5d09594a62bcb0c9752779d9/125:94/w_1994,h_1500,c_limit/Transpo_G70_TA-518126.jpg"
                            }}
                            style={{ height: 45, width: 60, borderRadius: 5 }}
                        />
                    </View>
                    <View style={{ width: "45%", marginLeft: 25 }}>
                        <Text>Four wheeler</Text>
                        <Text>A-15 Cb road | EV</Text>
                    </View>
                    <View style={{ width: "20%", alignItems: "flex-end" }}>
                        <TouchableOpacity>
                            <Icon name="trash" size={25} />
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableOpacity style={styles.addcontainer}>
                    <Text style={styles.text2}>Add more</Text>
                    <Icon name="add" size={25} style={{ marginLeft: 10 }} />
                </TouchableOpacity>
            </View>
            <View style={styles.btncontainer}>
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.btntext}>Continue</Text>
                    <Icon name="chevron-forward-outline" size={25} />
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        alignSelf: "flex-end",
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        // borderTopLeftRadius:40,
        // borderTopRightRadius:40,
        justifyContent: "space-around",
        alignItems: "center",
        backgroundColor: "white" //#6B8E23
    },
    text: {
        fontWeight: "bold",
        fontSize: responsiveFontSize(4)
    },
    text2: {
        // fontWeight:'bold',
        fontSize: responsiveFontSize(4)
    },
    namecontainer: {
        width: responsiveWidth(90),
        height: responsiveHeight(10),
        alignItems: "flex-end",
        flexDirection: "row",
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between"
    },
    btncontainer: {
        width: responsiveWidth(90),
        height: responsiveHeight(20),
        // backgroundColor:'black',
        alignItems: "center",
        justifyContent: "center"
    },
    button: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        height: responsiveHeight(9),
        width: responsiveWidth(80),
        backgroundColor: "#4169E1",
        borderRadius: 10
    },
    btntext: {
        fontSize: responsiveFontSize(4),
        color: "white"
    },
    midcontainer: {
        width: responsiveWidth(80),
        height: responsiveHeight(60)
    },
    card: {
        width: responsiveWidth(80),
        height: "15%",
        flexDirection: "row",
        // backgroundColor:'pink',
        // justifyContent:'center',
        alignItems: "center"
    },
    addcontainer: {
        width: responsiveWidth(80),
        height: "10%",
        marginTop: 25,
        alignItems: "center",
        flexDirection: "row"
    },
});

export default ParkingSpots;
