import React, { useEffect, useState, createRef } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
// import { Provider } from 'react-redux';
// import { store, persistor } from './src/Redux/store';
// import { PersistGate } from 'redux-persist/integration/react';
// import { persistStore } from 'redux-persist';
import Navigation from '../../../src/Routes/Navigation';
import { View, Text, Image, ScrollView, Switch, TextInput, SafeAreaView } from 'react-native'
import Fn from 'react-native-vector-icons/FontAwesome5'
import HomeIcon from '../../assets/IconComponent/HomeIcon';
import Ant from 'react-native-vector-icons/AntDesign'
import { MAP_KEY } from '../../../Config';
import {
  Button,
  Footer,
  FooterTab,
  Icon,
  Thumbnail,
  Badge,
  Input,
} from 'native-base';
import OnlineIcon from '../../assets/IconComponent/OnlineIcon';
import OfflineIcon from '../../assets/IconComponent/OfflineIcon';
import { grey } from 'ansi-colors';
import { TouchableOpacity } from 'react-native';
import ActionSheet from "react-native-actions-sheet";
import { Calendar, LocaleConfig } from 'react-native-calendars';
import MapViewDirections from 'react-native-maps-directions';
import Wrapped from '../../Components/Global/Wrapped';
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps"
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { windowWidth } from '../../Util/Dimensions';
import { RadioButton } from 'react-native-paper'; 
import { windowHeight } from '../../Util/Dimensions';

const actionSheetRef = createRef();

const SignUp = () => {
  const [showButton, setShowButton] = React.useState(false)
  let actionSheet;
  const [checked, setChecked] = React.useState('first');
  const [origin, setorigin] = useState()
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  const toggleSwitch2 = () => setIsEnabled2(previousState => !previousState);
  const [isEnabled2, setIsEnabled2] = useState(false);
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={{ backgroundColor: '#FFF' }}>
          <View>
            <Image resizeMode="contain"
              style={{ width: '100%', }}
              source={require('../../assets/login/loginImg.jpeg')}
            />
          </View>
          <View style={{ padding: 10 }}>
            <Text style={{ fontSize: 35, fontWeight: 'bold' }}>Sign up</Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <TouchableOpacity style={{
              backgroundColor: '#FFF', alignItems: 'center',
              borderColor: '#ECECEC', borderWidth: 1, width: '25%', borderRadius: 20
            }}>
              <Image resizeMode="center" source={require('../../assets/login/gmail.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={{ backgroundColor: '#FFF', borderColor: '#ECECEC', alignItems: 'center', borderWidth: 1, width: '25%', borderRadius: 20 }}>
              <Image resizeMode="center" source={require('../../assets/login/fb.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={{ backgroundColor: '#FFF', borderColor: '#ECECEC', alignItems: 'center', borderWidth: 1, width: '25%', borderRadius: 20 }}>
              <Image resizeMode="center" source={require('../../assets/login/ios.png')} />
            </TouchableOpacity>
          </View>
          <View style={{ padding: 20 }}>
            <TextInput
              style={{ height: windowHeight / 16, borderBottomWidth: 1, borderBottomColor: '#ECECEC' }}
              placeholder="Full Name"
            />
            <TextInput
              style={{ height: windowHeight / 16, borderBottomWidth: 1, borderBottomColor: '#ECECEC' }}
              placeholder="Age"
            />
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>
            <RadioButton
              color="#1D91CA"
              value="first"
              status={checked === 'first' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('first')}
            /><Text>Man</Text>

            <RadioButton
              color="#1D91CA"
              value="second"
              status={checked === 'second' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('second')}
            /><Text>Women</Text>

            <RadioButton
              color="#1D91CA"
              value="third"
              status={checked === 'third' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('third')}
            /><Text>Others</Text>
          </View>
          <View style={{ padding: 20, marginTop: 20 }}>
            <TouchableOpacity style={{
              flexDirection: 'row', backgroundColor: '#1D91CA', padding: 10, borderRadius: 10,
              justifyContent: 'center', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'
            }}>
              <Text style={{ color: 'white', fontSize: 28, marginLeft: 25 }}>Continue</Text>
              <Fn name="arrow-right"
                style={{ color: 'white', marginRight: 30 }}
                size={25} />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>

  );
};

export default SignUp;