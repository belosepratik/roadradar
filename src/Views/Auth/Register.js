import React from 'react';
import {
  Image,
  View,
  SafeAreaView,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
  Text,
  TextInput,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Input from '../../Components/Global/Input';
import Wrapped from '../../Components/Global/Wrapped';
import Fontisto from 'react-native-vector-icons/Fontisto';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useDispatch} from 'react-redux';
import {RegisterUser} from '../../Axios/axios';

export default function Register({navigation}) {
  const [registerData, setRegisterData] = React.useState({
    name: '',
    email: '',
    password: '',
    phone_no: '',
    username: '',
  });

  console.log(registerData);

  const dispatch = useDispatch();

  const handleClick = async () => {
    const res = await dispatch(RegisterUser(registerData));
    console.log(res, 'Register Response');
    alert('Sucessfully Added');
    navigation.goBack();
  };

  return (
    <Wrapped>
      <ScrollView>
        <View style={{alignItems: 'center', marginTop: 20}}>
          <Image
            source={require('../../assets/logo.jpg')}
            style={{width: 200, height: 200}}
          />
        </View>

        <View style={{}}>
          <Text style={{fontSize: 25, fontWeight: 'bold', alignSelf: 'center'}}>
            Road Radar
          </Text>
        </View>

        <View style={{backgroundColor: '#F6F0F0 ', margin: 20}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="user"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Name *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(text) =>
                setRegisterData({...registerData, name: text})
              }
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="user"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="User Name *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(text) =>
                setRegisterData({...registerData, username: text})
              }
            />
          </View>
          {/* <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20, marginTop: 20 }}>Login</Text> */}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <Fontisto
              name="email"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Email *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(text) =>
                setRegisterData({...registerData, email: text})
              }
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="lock"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Password *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(text) =>
                setRegisterData({...registerData, password: text})
              }
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <Ionicons
              name="call-outline"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Phone Number *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(text) =>
                setRegisterData({...registerData, phone_no: text})
              }
            />
          </View>

          <TouchableOpacity
            style={{
              margin: 20,
              padding: 10,
              backgroundColor: '#ffdc4a',
              borderRadius: 10,
            }}
            onPress={handleClick}>
            <Text style={{alignSelf: 'center', fontSize: 20}}>REGISTER</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{margin: 0}}
            onPress={() => navigation.goBack()}>
            <Text style={{alignSelf: 'center', fontSize: 15}}>
              Already registered ? Go to Login
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Wrapped>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 25,
    padding: 15,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  contentCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: 'white',
    padding: 10,
  },
  txtInputStyle: {
    fontSize: 20,
    margin: 5,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderColor: 'black',
    color: 'gray',
  },
});
