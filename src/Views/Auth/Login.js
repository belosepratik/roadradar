import React from 'react';
import {
  KeyboardAvoidingView,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';
import {Button} from 'react-native-paper';
import {useDispatch} from 'react-redux';
import InputForm from '../../Components/Form/InputForm';
import {setIsLogged} from '../../Redux/Slice/LoginSlice';

import Input from '../../Components/Global/Input';
import Wrapped from '../../Components/Global/Wrapped';
import Fontisto from 'react-native-vector-icons/Fontisto';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {loginUser} from '../../Axios/axios';

export default function Login({navigation}) {
  const dispatch = useDispatch();
  const submitHandler = (values) => {
    console.log(values);
    dispatch(setIsLogged(true));
    navigation.replace('Home');
  };

  const [loginData, setLoginData] = React.useState({
    email: '',
    password: '',
  });

  console.log(loginData);

  const handleClick = async () => {
    const res = await dispatch(loginUser(loginData));
    console.log(res && res, 'Login Response');
    if(res.status == true)
    {
      navigation.replace('Home');
      alert('Login Succesfull');
    }else
    {
      alert("Invalid Credentials")
    }
    // dispatch(setIsLogged(true));

    // alert('Login Succesfull');

    console.log(res, 'Outside');
    // navigation.replace('Home');
  };

  return (
    <Wrapped>
      <ScrollView>
        <View style={{alignItems: 'center', marginTop: 20}}>
          <Image
            source={require('../../assets/logo.jpg')}
            style={{width: 200, height: 200}}
          />
        </View>

        <View style={{}}>
          <Text style={{fontSize: 25, fontWeight: 'bold', alignSelf: 'center'}}>
            Road Radar
          </Text>
        </View>

        <View style={{backgroundColor: 'white', margin: 20}}>
          {/* <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20, marginTop: 20 }}>Login</Text> */}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <Fontisto
              name="email"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Email *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(text) => setLoginData({...loginData, email: text})}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 20,
              marginRight: 20,
              borderWidth: 0.2,
              borderRadius: 10,
              marginTop: 20,
            }}>
            <AntDesign
              name="lock"
              size={20}
              color="grey"
              style={{alignItems: 'center', marginLeft: 10}}
            />
            <Input
              placeholder="Password *"
              style={{marginLeft: 10, width: '80%'}}
              onChangeText={(text) =>
                setLoginData({...loginData, password: text})
              }
            />
          </View>
          <TouchableOpacity
            onPress={() => {
              handleClick();
            }}
            style={{
              margin: 20,
              padding: 10,
              backgroundColor: '#ffdc4a',
              borderRadius: 10,
            }}>
            <Text style={{alignSelf: 'center', fontSize: 20}}>LOGIN</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{margin: 10}}>
            <Text style={{alignSelf: 'center', fontSize: 15}}>
              Forgot Password?
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{margin: 10}}
            onPress={() => navigation.navigate('Register')}>
            <Text style={{alignSelf: 'center', fontSize: 15}}>
              Don't have an acount? Create here
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Wrapped>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    height: 150,
    width: 150,
    resizeMode: 'cover',
  },
  text: {
    fontFamily: 'Kufam-SemiBoldItalic',
    fontSize: 28,
    marginBottom: 10,
    color: '#051d5f',
  },
  navButton: {
    marginTop: 15,
  },
  forgotButton: {
    marginVertical: 10,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#2e64e5',
    fontFamily: 'Lato-Regular',
  },
});
