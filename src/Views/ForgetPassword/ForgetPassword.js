import React from "react";
import {
  Alert,
  Button,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from "react-native";
import {
  responsiveHeight as rH,
  responsiveWidth as rW,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import { Formik } from "formik";

export default function ForgetPassword() {
  return (
    <View style={styles.container}>
      <Formik
        initialValues={{ email: "",}}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        {(props) => (
          <View style={styles.forms}>
            <Text style={styles.text}>Forgot Password?</Text>
            <Text style={{ color: "grey", marginLeft: 15 }}>
              Enter your Email Address below to reset password
            </Text>
            <TextInput
              style={styles.input}
              placeholder="enter email"
              onChangeText={props.handleChange("email")}
              value={props.values.email}
            />
           
                   
            <View style={styles.inside}>
              <TouchableOpacity>
                <Button
                  title="Send email"
                  color="lightblue"
                  onPress={props.handleSubmit}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
      </Formik>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    // flex: 1,
    // backgroundColor: "#fff",
    marginTop: 150,
    justifyContent: "center",
    alignItems: "center",
    
  },
  text: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 15,
    color: "#333",
    fontSize: 24,
    fontWeight: "bold",
  },
  input: {
    // borderWidth: 1,
    borderBottomWidth: 2,
    height: rH(5),
    width: rW(80),
    borderColor: "#ddd",
    padding: 10,
    fontSize: 18,
    borderRadius: 6,
    margin: 10,
  },
  inside: {
    display: "flex",

    width: rW(85),
    height: rH(10),
    
    justifyContent: "center",
  },
  forms: {
    //   flex:1,
    //   alignItems: "center"
    margin: 20,
    display: "flex",
    justifyContent: "center",
  },
});
