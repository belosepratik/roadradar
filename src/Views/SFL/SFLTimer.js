import React, {useEffect, useState, createRef} from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
import {Provider, useSelector} from 'react-redux';
import {store, persistor} from '../../../src/Redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import {persistStore} from 'redux-persist';
import Navigation from '../../../src/Routes/Navigation';
import {View, Text, Image} from 'react-native';
import Fn from 'react-native-vector-icons/FontAwesome5';
import HomeIcon from '../../../src/assets/IconComponent/HomeIcon';
import Ant from 'react-native-vector-icons/AntDesign';
import {MAP_KEY} from '../.././../Config';
import {Button, Footer, FooterTab, Icon, Thumbnail, Badge} from 'native-base';
import OnlineIcon from '../../../src/assets/IconComponent/OnlineIcon';
import OfflineIcon from '../../../src/assets/IconComponent/OfflineIcon';
import {grey} from 'ansi-colors';
import {TouchableOpacity} from 'react-native';
import ActionSheet from 'react-native-actions-sheet';
import {Calendar, LocaleConfig} from 'react-native-calendars';
import MapViewDirections from 'react-native-maps-directions';
import Wrapped from '../../../src/Components/Global/Wrapped';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {windowWidth} from '../../../src/Util/Dimensions';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import { BackButton } from '../../Components/Universal/BackButton';
const actionSheetRef = createRef();
const SFLTime = ({navigation, route}) => {
  console.log('SFLTime Current >>>>>>>>>>>', route.name);
  console.log('SFLTime Prev >>>>>>>>>>>', route.params.backScreen);

  const home = useSelector((state) => state.home);
  const {flow} = home;

  const [showButton, setShowButton] = React.useState(false);
  let actionSheet;
  const homePlace = {
    description: 'Home',
    geometry: {location: {lat: 48.8152937, lng: 2.4597668}},
  };
  const workPlace = {
    description: 'Work',
    geometry: {location: {lat: 48.8496818, lng: 2.2940881}},
  };
  const wifi1 = <HomeIcon fill={'black'} width={30} height={30} />;
  const off = <OfflineIcon fill={'black'} width={30} height={30} />;
  const wifi = <OnlineIcon fill={'black'} width={30} height={30} />;
  const tomorrow = {date: 23, month: 'july', year: 'April'};

  const [origin, setorigin] = useState();
  LocaleConfig.defaultLocale = 'fr';
  const from = {address: 'thane west'}; //////location from
  const to = {address: 'mulund west'}; //////location to

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (time) => {
    console.warn('A time has been picked: ', time);
    hideDatePicker();
  };

  useEffect(() => {
    actionSheetRef.current?.setModalVisible();
  }, []);

  return (
    <Wrapped>
      <BackButton navigation={navigation} route={route} />
      <MapView
        style={{flex: 1}}
        showsUserLocation={true}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 19.2068505,
          longitude: 72.9528205,
          latitudeDelta: 0.0222,
          longitudeDelta: 0.0121,
        }}></MapView>
      <View
        style={{
          justifyContent: 'flex-start',
          flex: 1,
        }}>
        {/* {showButton && (
        <View style={{justifyContent: 'flex-start'}}>
          <TouchableOpacity style={{backgroundColor:'#FFFFFF',width:'18%',height:'30%',borderRadius:10,
      alignItems: 'center',justifyContent:'center'}}>
        <Fn
        name="arrow-left" size={30} />
      </TouchableOpacity>
        </View>
      )}
      <TouchableOpacity
        onPress={() => {
          actionSheetRef.current?.setModalVisible();
          setShowButton(true);
        }}>
        <Text style={{textAlign: 'center', padding: 30}}>Open ActionSheet</Text>
      </TouchableOpacity> */}

        <ActionSheet
          ref={actionSheetRef}
          containerStyle={{backgroundColor: 'white', borderRadius: 35}}>
          <View
            style={{backgroundColor: 'white', borderRadius: 35, padding: 20}}>
            <Text
              style={{fontSize: responsiveFontSize(2.8), fontWeight: '900'}}>
              Schedule for later
            </Text>
            <Text style={{fontSize: responsiveFontSize(2.8), color: 'grey'}}>
              Choose time
            </Text>
            <View
              style={{
                padding: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                style={{
                  backgroundColor: '#1E8FE7',
                  padding: 10,
                  borderRadius: 10,
                  height: '30%',
                  width: '90%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}
                onPress={showDatePicker}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2.8),
                    fontWeight: '800',
                    color: 'white',
                  }}>
                  Select Time
                </Text>
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="time"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
            </View>
            <View
              style={{
                justifyContent: 'flex-start',
                flexDirection: 'row',
                padding: 30,
                alignItems: 'center',
              }}>
              <Text style={{fontSize: responsiveFontSize(2.3)}}>
                <Fn name="circle" color="#1E8FE7" size={19} />
                Time -{' '}
              </Text>
              <Text style={{fontSize: responsiveFontSize(2.3), color: 'grey'}}>
                {/* {time=handleConfirm} */}
              </Text>
            </View>

            <View
              style={{
                justifyContent: 'flex-start',
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginLeft: 32,
              }}>
              <Text style={{fontSize: responsiveFontSize(2.3)}}>
                <Fn name="dot-circle" size={25} color="red" /> {from.address}{' '}
              </Text>

              <Fn
                name="ellipsis-v"
                size={20}
                color="black"
                style={{alignSelf: 'flex-start', marginLeft: 5}}
              />

              <Text style={{fontSize: responsiveFontSize(2.3)}}>
                <Fn name="map-marker-alt" size={28} color="#1E8FE7" />{' '}
                {to.address}{' '}
              </Text>
            </View>
            <View
              style={{marginTop: 20, alignItems: 'center', marginBottom: -170}}>
              <TouchableOpacity
                onPress={() => {
                  switch (flow) {
                    case 'Drive':
                      navigation.navigate('OnVehicle', {
                        backScreen: route.name,
                      });
                      break;
                    case 'Schedule Drive':
                      navigation.navigate('OnCarpoolAdd', {
                        backScreen: route.name,
                      });
                      break;
                    case 'Ride':
                      navigation.navigate('OnDestinatination', {
                        backScreen: route.name,
                      });
                      break;
                    case 'Schedule Ride':
                      navigation.navigate('NearBy', {backScreen: route.name});
                      break;
                  }

                  actionSheetRef.current?.hide();
                }}
                style={{
                  backgroundColor: '#1E8FE7',
                  padding: 10,
                  borderRadius: 10,
                  height: '30%',
                  width: '90%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2.8),
                    color: 'white',
                    marginLeft: 25,
                  }}>
                  Continue
                </Text>
                <Fn
                  name="arrow-right"
                  style={{color: 'white', marginRight: 30}}
                  size={30}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ActionSheet>
      </View>
    </Wrapped>
  );
};

export default SFLTime;
