import {createSlice} from '@reduxjs/toolkit';
import axios from 'axios';

export const homeslice = createSlice({
  name: 'home',
  initialState: {
    myLocation: '',
    error: null,
    loading: false,
    DestLocation: null,
    destlatlong: null,
    onboardingStatus: false,
    flow: 'Drive',
    flowSchedule: ""
  },
  reducers: {
    setFlow: (state, action) => {
      state.flow = action.payload;
    },
    setFlowSchedule: (state, action) => {
      state.flowSchedule = action.payload;
    },
    setOnBoardingStatus: (state, action) => {
      state.onboardingStatus = action.payload;
    },
    setmyLocation: (state, action) => {
      state.myLocation = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setDestLocation: (state, action) => {
      state.DestLocation = action.payload;
    },
    setdestlatlong: (state, action) => {
      state.destlatlong = action.payload;
    },
  },
});

//actions
export const {
  setFlow,
  setOnBoardingStatus,
  setFlowSchedule,
  setmyLocation,
  setError,
  setLoading,
  setDestLocation,
  setdestlatlong,
} = homeslice.actions;

export default homeslice.reducer;
