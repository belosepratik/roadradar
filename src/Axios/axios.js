import axios from 'axios';
import {setError, setLoading} from '../Redux/Slice/HomeSlice';

//add case
export const createData =
  (values, path, port = null) =>
  async (dispatch) => {
    console.log(values, path, port);
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      // dispatch(setLoading(true))
      // dispatch(setError(null))
      const {data} = await axios.post(
        // `http://web01.exits.in:${port}/api/${path}`,
        // `https://admin.ivatle.com/api/auth/${path}`,
        `http://web01.exits.in:8000/api/${path}`,
        values,
        config,
      );
      console.log(data);
      if (data.success) {
        // dispatch(setLoading(false))
        return {
          status: true,
          message: 'Successful !!',
          data: data,
        };
      } else {
        // dispatch(setLoading(false))
        console.log('Failed');
        return {
          status: true,
          message: 'Something went wrong please try again !!',
          data,
        };
      }
    } catch (error) {
      console.log(error);
      console.log(error.message);
      console.log(error.response);
      console.log(error.response.data.message);
      // dispatch(
      //     setError(
      //         error.response && error.response.data.message
      //             ? error.response.data
      //             : error.message
      //     )
      // )

      // dispatch(setLoading(false))
      return {
        status: false,
        message:
          error.response && error.response.data.message
            ? error.response.data
            : error.message,
      };
    }
  };

export const getAllData =
  (path, port = null) =>
  async (dispatch) => {
    console.log(path, port);
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const {data} = await axios.get(
        `http://web01.exits.in:8000/api/${path}`,
        config,
      );
      console.log(data);
      if (data.success) {
        return {
          status: true,
          message: 'Successful !!',
          data: data,
        };
      } else {
        // dispatch(setLoading(false))
        console.log('Failed');
        return {
          status: true,
          message: 'Something went wrong please try again !!',
          data,
        };
      }
    } catch (error) {
      console.log(error);
      console.log(error.message);
      console.log(error.response);
      console.log(error.response.data.message);
      // dispatch(
      //     setError(
      //         error.response && error.response.data.message
      //             ? error.response.data
      //             : error.message
      //     )
      // )

      // dispatch(setLoading(false))
      return {
        status: false,
        message:
          error.response && error.response.data.message
            ? error.response.data
            : error.message,
      };
    }
  };

export const deleteDataById = (id, path, port, getData) => async (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  try {
    const {data} = await axios.delete(
      `http://web01.exits.in:8000/api/${path}/${id}`,
      config,
    );

    if (data.success) {
      return {
        status: true,
        message: 'Successful !!',
        data: data,
      };
    } else {
      // dispatch(setLoading(false))
      console.log('Failed');
      return {
        status: true,
        message: 'Something went wrong please try again !!',
        data,
      };
    }
  } catch (error) {
    dispatch(
      setError(
        error.response && error.response.data.message
          ? error.response.data
          : error.message,
      ),
    );
    dispatch(setLoading(false));
  }
};

//selectlogin grabs the login data

export const RegisterUser = (registerData) => async (dispatch) => {
  console.log('insideREgister');
  console.log('Fields hai :', registerData);
  // dispatch(setIsLogged({ login: "data", loggedIn: true }));
  try {
    dispatch(setLoading(true));
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const {data} = await axios.post(
      `http://web01.exits.in:8000/api/auth/signup`,
      {
        username: registerData.username,
        email: registerData.email,
        name: registerData.name,
        password: registerData.password,
        // role: registerData.role,
        roles: 'driver',
      },
      config,
    );

    console.log('', data);

    // if (data.success) {
    //   dispatch(setIsLogged({login: data, loggedIn: data.success}));
    // } else {
    //   // dispatch(setError({ error: "something went wrong please try again" }));
    //   console.log('something went wrong');
    // }

    if (data.success) {
      return {
        status: true,
        message: 'Successful !!',
        data: data,
      };
    } else {
      // dispatch(setLoading(false))
      console.log('Failed');
      return {
        status: true,
        message: 'Something went wrong please try again !!',
        data,
      };
    }

    // console.log(data);
    dispatch(setLoading(false));
  } catch (error) {
    if (error.response) {
      console.log(error.response.data);
      dispatch(setError(error.response.data));
    } else {
      dispatch(setError(error.message));
    }
    dispatch(setLoading(false));
  }
};

export const loginUser = (inputData) => async (dispatch) => {
  console.log(inputData);
  try {
    dispatch(setLoading(true));
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const {data} = await axios.post(
      `http://web01.exits.in:8000/api/auth/signin`,
      {username: inputData.email, password: inputData.password},
      config,
    );

    console.log(data);

    if (data.success) {
      // dispatch(setLoading(false))
      return {
        status: true,
        message: 'Successful !!',
        data: data,
      };
    } else {
      // dispatch(setLoading(false))
      console.log('Failed');
      return {
        status: true,
        message: 'Something went wrong please try again !!',
        data,
      };
    }
    // console.log(data);
    dispatch(setLoading(false));
  } catch (error) {
    if (error.response) {
      console.log(error.response.data);
      dispatch(setError(error.response.data));
    } else {
      dispatch(setError(error.message));
    }
    dispatch(setLoading(false));
  }
};
