import axios from "axios";
import { setError, setLoading } from "../Redux/Slice/commonSlice";

//add case
export const createData = (values, path, port = null) => async (dispatch) => {
    console.log(values, path, port);
    const config = {
        headers: {
            'Content-Type': 'application/json',
        },
    }

    try {
        dispatch(setLoading(true))
        dispatch(setError(null))
        const { data } = await axios.post(
            // `http://web01.exits.in:${port}/api/${path}`,
            `https://admin.ivatle.com/api/auth/${path}`,
            values,
            config
        )
        console.log(data)
        if (data.success) {
            dispatch(setLoading(false))
            return {
                status: true,
                message: 'Successful !!',
                data : data
            }
        } else {
            dispatch(setLoading(false))
            console.log("Failed");
            return {
                status: true,
                message: 'Something went wrong please try again !!',
                data,
            }
        }
    } catch (error) {
        console.log(error)
        console.log(error.message)
        console.log(error.response)
        console.log(error.response.data.message)
        dispatch(
            setError(
                error.response && error.response.data.message
                    ? error.response.data
                    : error.message
            )
        )

        dispatch(setLoading(false))
        return {
            status: false,
            message:
                error.response && error.response.data.message
                    ? error.response.data
                    : error.message,
        }
    }
}